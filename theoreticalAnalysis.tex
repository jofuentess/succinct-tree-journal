
In the {\tt PFEA} algorithm, lines~4--16 and 18--22 perform
$O(n)$ work and have $T_p = O(n/p)$ and span 
$T_\infty = O(1)$.  The whole computation here (and in Lines~18--22) could have
been formulated as a single parallel loop.  However, in the interest of limiting
scheduling overhead, we create only as many parallel threads as necessary,
similar to the {\tt PSTA} algorithm in Section~\ref{subsec:PSTA}.  Line~17
performs $O(n)$ work and has $T_p = O(n/p + \lg p)$ and span $O(\lg n)$.  This
gives a total work of $T_1 = O(n)$ and a span of $T_\infty = O(\lg n)$.  The
running time on $p$ cores is $T_p = O(n/p + \lg p)$.

The analysis of the {\tt PSTA} algorithm is done in three steps:
Lines 1--21 of Algorithm~\ref{algo:PSTA1} require $O(n)$ work and have
span~$O(1)$.
Line~22 requires $O(p)$ work and has span $O(\lg n)$ because we compute
a prefix sum over only $p$ values.
Lines~23--28 require $O(n)$ work and have span $O(1)$.
Lines~1--6 of Algorithm~\ref{algo:PSTA2} require $O(n/s)$ work and have
span $O(1)$.
Lines~7--10 require $O(p)$ work and have span $O(\lg n/s)$.
Algorithm \ref{algo:PSTA3} requires $O(\sqrt{2^w}\poly(w))$ work and
has span $O(1)$. As was defined in Section \ref{subsec:suctrees},
$w$ is the machine word size.
Thus, the total work of {\tt PSTA} is $T_1 = O(n + \lg p + \sqrt{2^w}\poly(w))$
and its span is $O(\lg n)$.
This gives a running time of $T_p = O(T_1/p + T_\infty) =
O(n/p + \lg p + \sqrt{2^w}\poly(w)/p)$ on $p$ cores\footnote{Notice that the
term $\lg n$ of the span is implicit in the term $n/p + \lg p$ of $T_{p}$. When
$p\leq n/\lg n \rightarrow n/p \geq \lg n$. When $p > n/\lg n \rightarrow \lg p
= \Theta(\lg n)$.}.
The speedup is $T_1/T_p = O\left(\frac{p(n+\sqrt{2^{w}}\poly(w))}{n+\sqrt{2^{w}}\poly(w)+p\lg
p}\right)$. Under the assumption that $p\ll n$, the speedup approaches
$O(p)$. Moreover, the
parallelism $T_1/T_{\infty}$ (the maximum theoretical speedup) of {\tt
PSTA} is $\frac{n+\sqrt{2^{w}}\poly(w)}{\lg n}$.

The {\tt PSTA} algorithm does not need any extra memory related to the use of
threads. Indeed, it just needs space proportional to the input size
and the space needed to schedule the threads. A work-stealing
scheduler, like the one used by the DyM model, exhibits at most a
linear expansion space, that is, $O(S_1p)$, where $S_1$ is the minimum
amount of space used by the scheduler for any execution of a
multithreaded computation using one core. This upper bound is
optimal within a constant factor
\cite{Blumofe:1999:SMC:324133.324234}. In summary, the working space
needed by our algorithm is $O(n\lg n+S_1p)$ bits. Since in our
algorithm the scheduler does not need to consider the input
size to schedule threads, $S_1=O(1)$. Thus, since in modern
machines it is usual that $p\ll n$, the scheduling space is negligible
and the working space is dominated by $O(n\lg n)$.

Note that in succinct data structure design, it is common to adopt the assumption that $w = \Theta(\lg n)$, and when constructing lookup tables, consider all possible bit vectors of length $(\lg n)/2$ (instead of $w/2$).
This guarantees that the universal lookup tables occupy only $o(n)$ bits.
Adopting the same strategy, we can simplify our analysis and obtain
$T_p = O(n/p + \lg p)$.

Thus, we have the following theorem:

\begin{theorem}\label{lem:lg}
A $(2n+o(n))$-bit representation of an ordinal tree on $n$ nodes and its
balanced parenthesis sequence can be computed with $O(n)$ work, $O(\lg n)$ span
and $O(n\lg n)$ bits of working space. This representation can support the
operations in Table \ref{tbl:operations} 
in $O(\lg n)$ time.
\end{theorem}
