Jacobson~\cite{j1989} was the first to propose the design of succinct data
structures.  He showed how to represent an ordinal tree on $n$ nodes
using $2n+o(n)$ bits so that computing the first child, next sibling
or parent of any node takes $O(\lg n)$ time in the bit probe
model.  Clark and Munro~\cite{cm1996} showed how to support the same
operations in constant time in the word RAM model with word size
$\Theta(\lg n)$.  Since then, much work has been done on succinct tree
representations, to support more operations, to achieve compression,
to provide support for updates, and so
on~\cite{mr1997,bdmr1999,grr2004,jss2012,ly2008,hms2012,fm2014,Navarro:2014:FFS:2620785.2601073}.
See \cite{rr2013} for a thorough survey.

Navarro and Sadakane~\cite{Navarro:2014:FFS:2620785.2601073}
proposed a succinct tree representation, referred to as
NS-representation throughout this paper, which was the first to
achieve a redundancy of $O(n/\lg^c n)$ bits for any positive constant
$c$.  The \emph{redundancy} of a data structure is the additional
space it uses above the information-theoretic lower bound.  While all
previous tree representations achieved a redundancy of $o(n)$ bits,
their redundancy was $\Omega(n \lg\lg n / \lg n)$ bits, that is, just
slightly sub-linear.  The NS-representation also supports a large
number of navigational operations in constant time (see Table \ref{tbl:operations});
only the work in \cite{hms2012,fm2014} supports two additional operations.
An experimental study of succinct trees~\cite{ACNSalenex10} showed that a
simplified version of the NS-representation uses less space than other
existing representations in most cases and performs most operations
faster.  In this paper, we provide a parallel algorithm for
constructing this representation.

\begin{table}[Ht]
\setlength{\tabcolsep}{0pt}
\begin{center}
\begin{tabular} {l@{\hspace{1em}}>{\raggedright\arraybackslash}p{7.2cm}}
\toprule
\textbf{Operation}                 & \textbf{Description}        \\
\midrule
$\child(x,i)$                      & Find the $i$th child of node $x$\\
$\childrank(x)$                    & Report the number of left siblings of node $x$\\
$\degreeop(x)$                       & Report the degree of node $x$\\
$\depth(x)$                        & Report the depth of node $x$\\
$\levelanc(x,i)$                   & Find the ancestor of node $x$ that is $i$ levels above node $x$\\
$\subtreesize(x)$                  & Report the number of nodes in the subtree rooted at node $x$\\
$\height(x)$                       & Report the height of the subtree rooted at $x$\\
$\deepestnode(x)$                  & Find the deepest node in the subtree rooted at node $x$\\
$\lca(x,y)$                        & Find the lowest common ancestor of nodes $x$ and $y$ \\
$\lmostleaf(x)$ /$\rmostleaf(x)$   & Find the leftmost/rightmost leaf of the subtree rooted at node $x$\\
$\leafrank(x)$                     & Report the number of leaves before node $x$ in preorder\\
$\leafselect(i)$                   & Find the $i$th leaf from left to right\\
$\prerank(x)$/$\postrank(x)$       & Report the number of nodes preceding node $x$ in preorder/postorder\\
$\preselect$/$\postselect(i)$      & Find the $i$th node in preorder/postorder\\       
$\levellmost(i)$/$\levelrmost(i)$  & Find the leftmost/rightmost node among all nodes at depth $i$\\
$\levelsucc(x)$/$\levelpred(x)$    & Find the node immediately to the left/right of node $x$ among all nodes at depth $i$\\
\midrule
$\access(i)$                       & Report $P[i]$\\
$\findopen(i)$/$\findclose(i)$     & Find The matching parenthesis of $P[i]$\\
$\enclose(i)$                      & Find the closest enclosing matching parenthesis pair for $P[i]$\\
$\rankopen(i)$/$\rankclose(i)$     & Report the number of opening/closing parentheses in $P[1..i]$\\
$\selectopen(i)$/$\selectclose(i)$ & Find the $i$th opening/closing parenthesis\\
\bottomrule
\end{tabular}
\caption{Operations supported by the NS-representation~\cite{Navarro:2014:FFS:2620785.2601073}, including operations over the corresponding balanced parenthesis sequence.}
\label{tbl:operations}
\end{center}
\end{table}


\subsubsection{Simplified NS-representation}
\label{subsub:simpleNSrep}
The NS-representation is based on the balanced parenthesis sequence
$P$ of the input tree $T$, which is obtained by performing a preorder
traversal of $T$ and writing down an opening parenthesis when visiting a node
for the first time and a closing parenthesis after visiting all
its descendants. Thus, the length of $P$ is $2n$. See Figure \ref{fig:bp} as an
example.

\begin{figure}[t]
  \centerline{\includegraphics[scale=0.55]{./images/bp.eps}}
  \caption{Balanced parentheses representation $P$ of a tree $T$. This representation,
  also known as folklore encoding, can be stored using a bit vector, writing a
  $1$ for each opening parenthesis and a $0$ for each closing parenthesis.}
  \label{fig:bp}
\end{figure}

The NS-representa\-tion is not the first structure to use balanced
parentheses to represent trees.  Munro and Raman~\cite{mr1997} used
succinct representations of balanced parentheses to represent ordinal trees
and reduced a set of navigational operations on trees to operations on their
balanced parenthesis sequences.  Their solution supports only a subset of the
operations supported by the NS-representation.  Additional operations
can be supported using auxiliary data structures~\cite{ly2008,Sadakane:2007:CST:1326296.1326297,MUNRO2001205,Munro2004Function}, but
    supporting all operations in Table \ref{tbl:operations}
    requires many
auxiliary structures, which increases the size of the final data structure
and makes it complex in both theory and practice.  The main novelty of
the NS-representation lies in its reduction of a large set of
operations on trees and balanced parenthesis sequences to a small set
of \emph{primitive operations}.  Representing $P$ as a bit vector
storing a $1$ for each opening parenthesis and a $0$ for each closing
parenthesis (see Figure \ref{fig:bp}), these primitive operations are the following, where $g$ is an
arbitrary function on $\{0,1\}$:
\begin{align*}
  \sumop(P,g,i,j) &= \textstyle\sum_{k=i}^jg(P[k])\\
  \fwdsearch(P,g,i,d) &= \min\{j \mid j \ge i, \sumop(P,g,i,j) = d\}\\
  \bwdsearch(P,g,i,d) &= \max\{j \mid j \le i, \sumop(P,g,j,i) = d\}\\
  \rmq(P,g,i,j) &= \min\{\sumop(P,g,i,k) \mid i\le k\le j\}\\
  \RMQ(P,g,i,j) &= \max\{\sumop(P,g,i,k) \mid i\le k\le j\}\\
  \rmqi(P,g,i,j) &= \argmin_{k\in[i,j]}\{\sumop(P,g,i,k)\}\\
  \RMQi(P,g,i,j) &= \argmax_{k\in[i,j]}\{\sumop(P,g,i,k)\}
\end{align*}
Most operations supported by the NS-representation reduce to these primitives
by choosing $g$ to be one of the following three functions:
\begin{align*}
  \pi : 1 &\mapsto 1 &\phi : 1 &\mapsto 1 & \psi : 1 &\mapsto 0\\
  0 &\mapsto -1 & 0 &\mapsto 0 & 0 &\mapsto 1
\end{align*}
For example, assuming the $i$th parenthesis in $P$ is an opening parenthesis,
the matching closing parenthesis can be found using
$\fwdsearch(P,\pi,i,0)$.  Thus, it (almost)\footnote{A few
  navigational operations cannot be expressed using these primitives.
  The NS-representation includes additional structures to support
  these operations.} suffices to support the primitive operations above for
$g \in \{\pi, \phi, \psi\}$.  To do so, Navarro and Sadakane designed
a simple data structure called \emph{Range Min-Max Tree} ({\tt RMMT}),
which supports the primitive operations above in logarithmic time when used
to represent the entire sequence~$P$.  To achieve constant-time
operations, $P$ is partitioned into chunks.  Each chunk is represented
using an {\tt RMMT}, which supports primitive operations inside the
chunk in constant time if the chunk is small enough.  Additional data
structures are used to support operations on the entire sequence $P$
in constant time.

\begin{figure}[t]
  \centerline{\includegraphics[scale=0.55]{./images/rmmt.eps}}
  \caption{Range min-max tree of the balanced parentheses sequence of the
  Figure \ref{fig:bp}, with $s=7$. In the figure, the $m'$ and $M'$ values involved in the
  operation $\fwdsearch(P,\pi,5,1)=20$ are underlined.}
  \label{fig:RangeMinMaxTree}
\end{figure}

Next we briefly review the {\tt RMMT} structure and how it supports
the primitive operations for $g = \pi$ (see Figure \ref{fig:bp} for an example
of function $\pi$).  Navarro and
Sadakane~\cite{Navarro:2014:FFS:2620785.2601073} discussed how to make
it support these operations also for $\phi$ and $\psi$ while
increasing its size by only $O(n/\lg^c n)$.  To define the variant of
the {\tt RMMT} we implemented, we partition $P$ into chunks
of size $s = w \lg n$, where $w$ is the machine word size.  For
simplicity, we assume that the length of $P$ is a multiple of~$s$.
The {\tt RMMT} is a complete binary tree over the sequence of
chunks (see Figure \ref{fig:RangeMinMaxTree}).
(If the number of chunks is not a power of $2$, we pad the sequence
with chunks of zeroes to reach the closest power of $2$.
These chunks are not stored explicitly.)
Each node $u$ of the {\tt RMMT} represents a subsequence $P_u$
of $P$ that is the concatenation of the chunks corresponding to the
descendant leaves of $u$.  Since the {\tt RMMT} is a complete tree, we
need not store its structure explicitly.  Instead, we index its
nodes as in a binary heap and refer to each node by its index.  The
representation of the {\tt RMMT} consists of four arrays $e'$, $m'$,
$M'$, and $n'$, each of length equal to the number of nodes in the
{\tt RMMT}.  The $u$th entry of each of these arrays stores some
crucial information about $P_u$: Let the {\em excess} at position $i$
of $P$ be defined as $\sumop(P,\pi,0,i) = \sum_{k=0}^{i} \pi(P[k])$.
$e'[u]$ stores the excess at the last position in $P_u$.  $m'[u]$ and
$M'[u]$ store the minimum and maximum excess, respectively, at any position in
$P_u$.  $n'[u]$ stores the number of positions in $P_u$ that
have the minimum excess value $m'[u]$.

Combined with a standard technique called {\em table lookup}, an {\tt RMMT}
supports the primitive operations
for $\pi$ in $O(\lg n)$ time.  Consider
$\fwdsearch(P,\pi,i,d)$ for example.  We first check the chunk containing $P[i]$
to see if the answer is inside this chunk.  This takes
$O(\lg n)$ time by dividing the chunk into portions of length $w/2$
and testing for each portion in turn whether it contains the answer.
Using a lookup table whose content does not
depend on $P$, the test for each portion of length $w/2$ takes
constant time: For each possible bit vector of length $w/2$ and each
of the $w/2$ positions in it, the table stores the answer
of $\fwdsearch(P,\pi,i,d)$ if it can be found inside this bit vector,
or $-1$ otherwise.  As there are $2^{w/2}$ bit vectors of length
$w/2$, this table uses $2^{w/2}\poly(w)$ bits.  If we find the answer
inside the chunk containing~$P[i]$, we report it.
Otherwise, let $u$ be the leaf corresponding to this
chunk.  If $u$ has a right sibling, we inspect the sibling's $m'$ and
$M'$ values to determine whether it contains the answer.  If so,
let $v$ be this right sibling.  Otherwise, we move up the tree from
$u$ until we find a right sibling $v$ of an ancestor of $u$ whose
corresponding subsequence $P_v$ contains the query answer.  Then we
use a similar procedure to descend down the tree starting from $v$ to
look for the leaf descendant of $v$ containing the answer and spend
another $O(\lg n)$ time to determine the position of the answer
inside its chunk. Since we spend $O(\lg n)$ time for each of
the two leaves we inspect and the tests for any other
node in the tree take constant time, the cost is $O(\lg
n)$.

Figure \ref{fig:RangeMinMaxTree} shows the $m'$ and $M'$ values
involved in the answer of $\fwdsearch(P,\pi,5,1)$. In this particular example,
the objective is to find the closest position after $i=5$ with excess
value $d=1$. Using lookup tables, we check if the answer is in the range
$[5,6]$ of the chunk $\lfloor 5/7\rfloor=0$. Since the answer is not there, we
analyze the right sibling of the chunk $0$. The $m'$ and $M'$ values of the right
sibling are $2$ and $4$, so the answer is not there. We now move to the parent
of the parent of the chunk $0$. Let's call $v$ to such node. The $m'$ and $M'$
values of $v$ are $0$ 
and $5$, and therefore, the answer exists and it is in the right child of $v$. Then,
we check the $m'$ and $M'$ values of the child of $v$. Those values are $1$ and
$5$, therefore, we need to move to the left child of the right child of
$v$. Since the current node is a leaf, we use lookup tables to find the first
value $1$ is that chunk. In this case, such $1$ value is at position $20$.

Supporting operations on the leaves, such as finding the $i$th leaf
from the left, reduces to $\rankop$ and $\selop$ operations over
a bit vector $P_1[1..2n]$ where $P_1[i] = 1$ iff $P[i] = 1$ and
$P[i+1] = 0$.  $\rankop$ and $\selop$ operations over $P_1$ in turn
reduce to $\sumop$ and $\fwdsearch$ operations over $P_1$ and can thus
be supported by an {\tt RMMT} for $P_1$.  $P_1$ does not need to be
stored explicitly because any consecutive $O(w)$ bits of $P_1$ can be
computed from the corresponding bits of $P$ using table lookup.

%% To analyze the space usage, observe that storing $P$ requires $2n$
%% bits, while the space needed to store the vectors $e'$, $m'$, $M'$,
%% and $n'$ is $2(n/s) \lg n = 2n/w$.  The space needed to store the same
%% vectors for the {\tt RMMT} of $P_1$ is the same.  Since we can assume
%% that $w = \Omega(\lg n)$, the total size of the {\tt RMMT} is thus
%% $2n + O(n / \lg n)$ bits.


\subsubsection{Constant time queries}
\label{subsub:O(1)time-rmMt}

To support constant time queries on arbitrary-sized trees, the balanced parentheses
representation $P$ needs to be partitioned into blocks. We represent each block
using a {\tt RMMT} and then construct additional data structures considering the
minimum, maximum and excess values of the {\tt RMMT} of each block. The size of
each block is $w^{c}$, so we have $\tau=\lceil 2n/w^{c}\rceil$ of such
blocks. To support constant time queries inside each block, we construct
a {\tt RMMT}, similar as before, but with $s=w/2$ and arity $k=\Theta
(w/c\lg w)$, instead of arity $2$. Let $m_{1},\ldots,m_{\tau}$,
$M_{1},\ldots,M_{\tau}$, $n_{1},\ldots,n_{\tau}$ and 
$e_{1},\ldots,e_{\tau}$ be the minima, maxima, number of minima and excess stored at the root of
the $\tau$ {\tt RMMT}s. Depending on the operations, the additional data
structures differ.

To solve $\fwdsearch(P,\pi,i,d)$, we first try to solve it inside
block $j=\lfloor i/w^{c}\rfloor$. The answer is returned if it is found in that
block. If it is not, we must find
the first excess $d^{\prime}=d+e_{j-1}+\sumop(P,\pi,0,i-1-w^{c}\cdot
(j-1))$ in the {\tt RMMT}s of the following blocks. Applying
Lemma 4.4 of \cite{Navarro:2014:FFS:2620785.2601073}, we must either find the
first block $r>j$ such that $m_{r}\leq d^{\prime}$, or such that
$M_{r}\geq d^{\prime}$. Once we find such a block, we complete the
query inside of it with a local $\fwdsearch(P,\pi,0,d^{\prime}-e_{r-1})$.

To find the corresponding block $r$ in constant time, the authors
propose additional data structures to represent the left-to-right minima and
maxima values. For the case of left-to-right minima, it is necessary
to build a tree called {\it 2d-Min-Heap} (The left-to-right maxima is similar):

\begin{defn}\cite{Navarro:2014:FFS:2620785.2601073}
Let $m_{1},\ldots,m_{\tau}$ be a sequence of integers. We define for each $1\leq
j\leq \tau$ the left-to-right minima starting at $j$ as
$lrm(j)=(j_0,j_1,\ldots)$ where $j_0=j$, $j_r<j_{r+1}$, $m_{j_{r+1}}<m_{j_{r}}$,
and $m_{j_r+1},\ldots, m_{j_{r+1}-1}\geq m_{j_r}$
\end{defn}

Once two $lrm$ sequences coincide, they do so until the end.
Thus, a 2d-Min-Heap is defined as a trie of $\tau$ nodes, composed of the reversed
$lrm$ sequences. Since the resulting trie can be composed of disconnected paths, a dummy
root is used to generate the tree. If we assign weight to the edges, where the
weight of an upward edge $(j_i, j_{i+1})$ is defined as $m_{j_i}-m_{j_{i+1}}$,
we can reduce the problem of finding the first block $r>j$ such that $m_r\leq
d^{\prime}$ to the a weighted level ancestor query over the 2d-Min-Heap. More
precisely, we need to find the first ancestor $j_r$ of node $j$ such that the
sum of the weights between $j$ and $j_r$ is greater than
$d^{\prime\prime}=m_j-d^{\prime}$. Figure \ref{fig:2dMinHeap-a} shows an
example of the 2d-Min-Heap for the sequence $(1,4,9,5,10,7,3,2,5,4)$.


\begin{figure}[t]
    \centering
    \begin{subfigure}[c]{0.48\textwidth}
        \centering
        \includegraphics[scale=.4]{./images/2dMinHeap-a.eps}
        \caption{{\em
  2d-Min-Heap} of the sequence $(1,4,9,5,10,7,3,2,5,4)$.}
        \label{fig:2dMinHeap-a}
    \end{subfigure}
    \quad
    \begin{subfigure}[c]{0.48\textwidth}
        \includegraphics[scale=.4]{./images/2dMinHeap-b.eps}
        \centering
        \vspace{1.4em}
        \caption{The {\em ladders} generated by the 2d-Min-Heap of
    Figure \ref{fig:2dMinHeap-a}.}
        \label{fig:2dMinHeap-b}
    \end{subfigure}
    \caption[2d-Min-Heap of the sequence $(1,4,9,5,10,7,3,2,5,4)$]{Example of
    the sequence $(1,4,9,5,10,7,3,2,5,4)$ and its ladders decomposition.  In the
    2d-Min-Heap, the indices of the sequence are inside and the values are outside of the nodes of the tree.}
    \label{fig:2dMinHeap}
\end{figure}

To answer the weighted level ancestor query, we need to decompose the
2d-Min-Heap. The 2d-Min-Heap is decomposed into paths by recursively extracting
the longest path. Then, for each path of length $l$, we store an extension of it
by adding at most $l$ nodes towards the root. These extended paths are
called \emph{ladders}. Figure \ref{fig:2dMinHeap-b} shows an example of
ladders. This decomposition ensures that a node with height $h$ will have its
first $h$ ancestors in its ladder. For
each ladder, a sparse bitmap is stored, where the $i$-th 1 of the bitmap
represents the $i$-th node upward in the ladder, and the distance
between two 1's is equal to the weight of the edge between them. All the bitmaps
are concatenated into one of size $O(n)$, which is represented by the
sparse bitmap of P\v{a}tra\c{s}cu
\cite{Patrascu:2008:SUC:1470582.1470670}. Additionally, for 
each node $v$ of the 2d-Min-Heap, the $\lg\tau$ ancestors at depths
$depth(v)-2^{i}$, $i\geq 0$ are stored in an array. Similarly,
for each node $v$, the $\lg\tau$ accumulated weights toward the
ancestors at distance $2^{i}$ are stored using {\em fusion
trees} \cite{Fredman1993424}. Fusion trees are used to store $z$ keys of $l$
bits each one in $O(zl)$ bits, supporting predecessor queries in $O(\lg_{l}z)$
time, by using a $l^{1/6}$-ary tree. The $1/6$ factor can be reduced to achieve
$O(1/\epsilon)$ predecessor query, where $0<\epsilon\leq 1/2$ \cite{Navarro:2014:FFS:2620785.2601073}.

Observe that there is no guarantee that the weighted level ancestor $j_r$ of the
node $j$ is in the ladder of $j$. Therefore, to answer the weighted level
ancestor query we need first to compute the ancestor $j'$ of node $j$ at distance
$2^{\lfloor \lg(depth(j)-d^{\prime\prime})\rfloor}$. The answer is in the ladder
of $j'$. The ancestor $j'$ can be founded in constant the by a predecessor query
of fusion tree of the node $j$ and the array
with the $\lg\tau$ ancestors of the node $j$. If $j'$ is at distance $2^i$, then
the answer is at distance less than $2^{i+1}$. Applying rank/select queries over
the bitmap of the ladder of node $j'$, we find the node $j_r$.


To solve $\rmqi(P,g,i,j)$ and $\RMQi(P,g,i,j)$ operations on the
$\tau$ blocks, we just need to build a data structure that supports range
minimum and maximum queries in constant time, such
as \cite{Fischer2007,Sadakane:2002:SRL:545381.545410}.

To solve $\degreeop(i)$ operations, we need to consider {\em pioneers}. Let {\em
pioneers} be the tighest matching pair of parentheses 
$(i,j)$, with $j=\findclose(i)$, such that $i$ and $j$ belong to
different blocks. Let's call a {\em marked} block is a block that has the opening
parenthesis of a pionner
$(i,j)$ such that $(i,j)$ contains a whole block, i.e., $i$ and $j$ do not belong to
consecutive blocks. Let $a$ be a marked block with pionner $(i,j)$ and let $b$ be a block, we say
that the block $a$ {\em contains} the block $b$ if the block $b$ is between the
blocks where $i$ and $j$ belong.
There are $O(\tau)$ of such
marked blocks. The $\degreeop(i)$
operation, number of children of a node $i$, can be solved as follows: If the operation involves at most
two consecutive blocks, then the answer can be computed in constant
time consulting the two corresponding {\tt RMMT}s. Otherwise, it corresponds to the
degree of a marked block. Since there are $O(\tau)$ of such blocks, we can
spend $O(\tau\lg n)$ bits to store explicitly the degree of all the marked
blocks and answer the operation in constant time.

The marked blocks are also used to solve $\child(i,q)$ and $\childrank(i)$
operations. Both for $\child(i,q)$ and $\childrank(i)$, if the block of $i$ is
not a marked block, then both can be solved in at most two in-block
queries. For marked blocks, we store a bitmap to represent the
information about the children of each of them. For each marked block $j$, we
store, in left-to-right order, information of marked blocks and blocks fully
contained in $j$. For each block $j'$ contained in block $j$, we store the
number of children of $j$ 
that starts within $j'$ (the number of minima of block $j'$) and for each
children-marked block, we store a $1$, which represents a block containing one
child of $j$. All numbers are stored in a bitmap as gaps of $0$'s  between consecutive $1$'s. For
the $\child(i,q)$ query, we first check if $\child(i,q)$ lies in the block of
$i$ or in $\findclose(i)$. If true, we solve it with an in-block query. If
not, we compute $p=\rankop_1(C_i,\selop_0(C_i,q))$, where $C_i$ is the
bitmap associated to the block of $i$. The value $p$ represents the position of
the block or marked block contained in $i$, where the $q$-th child of $i$
lies. If it is a marked block, then that is the answer. If it is a block $j$,
then the answer corresponds to the $q'$-th minimum within that block, where
$q'=q-\rankop_0(C_i,\selop_1(C_i,p))$. $\childrank(i)$ can be solved
similarly. Since the number of $1$'s on each bitmap is less than the number of
$0$'s, the bitmap can be stored using the sparse bitmap
of \cite{Patrascu:2008:SUC:1470582.1470670}.

The remaining operations require $\rankop$ and $\selop$ on $P$, or the virtual
bit vectors $P_{1}$ and $P_{2}$. For $\rankop$, it is necessary to store the
answers at the end of each block, finishing the query inside the corresponding
block. For $\selop_{1}$ (and $\selop_{0}$), we build a sequence with
the accumulated $1$'s in each of the $\tau$ blocks. Such sequence is stored in a
bitmap, representing each number in unary as gaps of $0$'s between consecutive $1$'s
using the results of \cite{Patrascu:2008:SUC:1470582.1470670}.



\subsubsection{Memory space}
To analyze the space used for the simplified NS-representation, observe that
storing $P$ requires $2n$ bits, while the space needed to store the vectors
$e'$, $m'$, $M'$, and $n'$ is $2(n/s) \lg n = 2n/w$.  The space needed to store
the same vectors for the {\tt RMMT} of $P_1$ is the same. Since we can assume that
$w = \Omega(\lg n)$, the total size of the simplified {\tt RMMT} is thus $2n + O(n
/ \lg n)$ bits.

The NS-representation that supports constant time queries requires the
construction of $\tau=\lceil 2n/w^{c}\rceil$ {\tt RMMT}s over sequences of
$w^{c}$ parentheses. Thus, the $\tau$ {\tt RMMT}s require $2n + O(n/\lg n)$ bits to
be stored. The additional data structures needed to support
constant time queries add some extra space: To support $\fwdsearch$, the ladders
use $O(\frac{n\lg n}{w^c})$ bits, the arrays of ancestors use $O(\frac{n\lg^{2}n}{w^c})$ bits,
the sparse bitmap uses $O(\frac{n\lg w^c}{w^{c}}+\frac{nt^{t}}{\lg^{t}n}+n^{3/4})$ bits and the fusion trees use $O(\frac{n\lg^{2}n}{w^c})$
bits. Thus, the extra structures to support $\fwdsearch$ uses $O(\frac{n\lg^{2}n}{w^{c}}+\frac{nt^{t}}{\lg^{t}n}+n^{3/4})$ bits,
with $t>0$. The $\rmqi$ and $\RMQi$ queries add $O(n/w^{c})$ extra bits. Since
there are $O(n/w^{c})$ marked blocks, the $\degreeop$ operation uses $O(n\lg
n/w^{c})$ extra bits. The bitmaps of the remaining operations, such as $\child$
and $\childrank$, uses
$\frac{2n}{w^{c}}\lg(w^{c})+O(\frac{nt^{t}}{\lg^{t}n}+n^{3/4})$ extra
bits, since they correspond to the sparse bitmap
of \cite{Patrascu:2008:SUC:1470582.1470670}. Therefore, the total space used by
the additional data structures is
$O(\frac{n(c\lg w+\lg^{2}n)}{w^{c}}
+ \frac{nt^{t}}{\lg^{t}n}+n^{3/4}+\sqrt{2^w})$ bits, where the term $\sqrt{2^w}$
corresponds to the lookup tables. With $w = \Omega(\lg n)$ and $t=c$, the extra
space is $O(\frac{n(c\lg\lg n+\lg^{2}n)+nc^c}{\lg^{c}n}
+n^{3/4}+\sqrt{n})$ bits. Combined with the $2n + O(n/\lg n)$ bits of
the {\tt RMMT}s, the NS-representation requires
$2n + O(n/\lg n + \frac{n(c\lg\lg n+\lg^{2}n)}{\lg^{c}n})$ bits, with $c>0$, supporting
queries in $O(c)$ time.

According to \cite{Navarro:2014:FFS:2620785.2601073}, the $O(n/\lg n)$ space of the {\tt RMMT}s can be
reduced by using {\em aB-trees} \cite{Patrascu:2008:SUC:1470582.1470670}. Given an
array $A$ of size $N$, with $N$ a power of $B$, an aB-tree is a complete tree of
arity $B$, that stores $B$ consecutive elements of $A$ on its leaves. Besides,
each node of the aB-tree stores a value $\varphi\in\Phi$. For the leaves,
$\varphi$ must be a function of the elements of $A$ that it stores; for internal
nodes, $\varphi$ must be a function of the $\varphi$-values of its children. An
aB-tree can decode the $B$ $\varphi$-values of the children of any internal node
and the $B$ values of $A$ for the leaves in constant time, if they are packed in
a machine word. An aB-tree can be stored in $N+2+O(\sqrt{2^w})$ bits
(See \cite{Patrascu:2008:SUC:1470582.1470670} for the details). Thus, with
$A=P$, $B=k=s=O(\frac{w}{c\lg w})$, $\varphi$-values encoding $e',m',M',n'$
values and blocks of size $N=B^{c}$, it is possible to store each {\tt RMMT} in
$N+2+O(\sqrt{2^w})$ bits. The sum of all the {\tt RMMT}s is $2n+O(\frac{n}{B^c}
+ \sqrt{2^w}) = 2n+O(\frac{n(c\lg\lg n)^c}{\lg^c n} + \sqrt{2^w})$. Finally,
using aB-trees to store the {\tt RMMT}s, the space usage of the NS-representation
is reduced to $2n +O(\frac{n(c\lg\lg n+\lg^{2}n)}{\lg^{c}n}+\sqrt{2^w})$ bits.
