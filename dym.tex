In the DyM model \cite[Chapter~27]{Cormen2009}, a {\em multithreaded
computation} is modelled as a directed acyclic graph $G=(V,E)$
whose vertices are instructions and where $(u,v) \in E$ if
$u$ must be executed before~$v$.  The time $T_p$ needed to execute the
computation on $p$ cores depends on two parameters of the computation:
its {\em work} $T_1$ and its {\em span} $T_\infty$.  The work is the
running time on a single core, that is, the number of nodes
(i.e., instructions) in $G$, assuming each instruction takes constant
time.  Since $p$ cores can execute only $p$ instructions at a time, we
have $T_p = \Omega(T_1/p)$.  The span is the length of the longest
path in~$G$.  Since the instructions on this path need to be executed
in order, we also have $T_p = \Omega(T_\infty)$.
Together, these two lower bounds give $T_p = \Omega(T_\infty +
T_1/p)$.
Work-stealing schedulers match the optimal bound to within
a factor of 2~\cite{Blumofe:1999:SMC:324133.324234}.  The degree to
which an algorithm can take advantage of the presence of $p > 1$ cores
is captured by its {\em speed-up} $T_1 / T_p$ and its {\em
parallelism} $T_1 / T_\infty$.  In the absence of cache effects, the
best possible speed-up is $p$, known as {\em linear speed-up}.
Parallelism provides an upper bound on the achievable speed-up.

To describe parallel algorithms in the DyM model, we augment
sequential pseudocode with three keywords.  The {\bf spawn} keyword,
followed by a procedure call, indicates that the procedure should run
in its own thread and {\em may} thus be executed in parallel to the
thread that spawned it.  The {\bf sync} keyword
indicates that the current thread must wait for the termination of all
threads it has spawned.  It thus provides a
simple barrier-style synchronization mechanism.  Finally, {\bf parfor}
is ``syntactic sugar'' for {\bf spawn}ing one thread per iteration in
a for loop, thereby allowing these iterations to run in parallel,
followed by a {\bf sync} operation that waits for all iterations to
complete. In practice, the {\bf parfor} keyword is implemented by
halving the range of loop iterations, {\bf spawn}ing one half and using the
current procedure to process the other half recursively until reaching one
iteration per range. After that, the iterations are executed in parallel. This
implementation adds an overhead to the parallel algorithm bounded above by the
logarithm of the number of loop iterations. When a procedure exits, it
implicitly performs a {\bf sync} to ensure all threads it spawned finish
first. If a stream of instructions does not contain one of the above keywords,
or a {\bf return } (which implicitly {\bf sync}’s) from a procedure, we group
these instruction into a single {\em strand}.

 \begin{figure}[t]
  \begin{minipage}[b]{0.47\textwidth}
    \begin{function}[H]
    \SetKwInOut{Input}{Input}
    \SetAlgoNoEnd
    \SetKw{Spawn}{spawn}
    \SetKw{Sync}{sync}
    \LinesNumbered
    \DontPrintSemicolon
      % I/o
      \Input{$A$, $v$, $s$, $e$}
      \BlankLine
      $c \asgn 0$\;
      \If{$\id{e}-\id{s} = 1$}{
      \lIf{$A[s] = v$}{\Return{1}}
         \Return{0}
      }
      $m \asgn \lfloor(s+e)/2\rfloor$\;
      $a \asgn$\Spawn{\id{pcount}($A,v,s,m$)}\;
      $b \asgn$\id{pcount}($A,v,m+1,e$)\;
      \Sync\;
      \Return{a+b}    
    \end{function}
  \end{minipage}%
  \hspace{\stretch{1}}%
  \begin{minipage}[c]{0.5\textwidth}
    \includegraphics[width=1.04\textwidth]{./images/dym}
  \end{minipage}\\[1ex]
  \leavevmode\begin{minipage}[t]{0.47\textwidth}
    \captionof{algocf}{pcount(). Example of a parallel recursive algorithm using the
      {\bf spawn} and {\bf sync } keywords. In parallel, the algorithm counts
  the occurrences of the element $v$ between the $s$-th and $e$-th elements of
  the subarray $A$.\label{algo:exampleAlgo}} 
  \end{minipage}%
  \hspace{\stretch{1}}%
  \begin{minipage}[t]{0.5\textwidth}
  \captionof{figure}{Example of a multithreaded computation on the Dynamic
    Multithreading Model. It corresponds to the Directed Acyclic Graph
    representation of the call $pcount(A,v,0,6)$ of the Algorithm
    \ref{algo:exampleAlgo}. Vertices represent strands and edges represent dependences.\label{fig:dym}}
  \end{minipage}
\end{figure}

For example, Algorithm \ref{algo:exampleAlgo} represents a parallel algorithm
using {\bf spawn} and {\bf sync}, and Figure \ref{fig:dym} shows its
multithreaded computation. In the figure, each circle represents one strand and
each rounded rectangle represents strands that belong to the same procedure
call. Let $A[s,e]$ be the subarray with elements $A[s],A[[s+1],\ldots,A[e]$. The
algorithm starts on the initial procedure call with the subarray $A[0,6]$. The
first half of the subarray is spawned (black circle in the initial call) and the
second half is processed by the same procedure (gray circle of the initial
call). This divide-and-conquer strategy is repeated until reaching strands with
one element of the array $A$ (black circles on the bottom of the figure, where
$s$ is equal to $e$). Once a bottom strand is finished, it syncs to its calling
procedure (white circles), until reaching the final strand (white circle of the
initial call). Assuming that each strand takes unit time, the work is 25 time
units and the span is 8 time units (this is represented in the figure by the
nodes connected with shaded edges). For more examples of the usage of the DYM
model, see \cite[Chapter~27]{Cormen2009}.
