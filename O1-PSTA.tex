In this section we show how to construct the {\it 2d-Min-Heap} and its
{\it ladders}, the {\it sparse bitmap} of P\v{a}tra\c{s}cu, {\it fusion trees}
and {\it range-minimum-query} structure in parallel, plus the computation of
marked blocks. All of these structures are
built over the minima, maxima, excess and the number of minima values of the
$\tau=\lceil 2n/w^{c}\rceil$ {\tt RMMT}s and are used to support different
operations over trees in constant time (see Section \ref{subsub:O(1)time-rmMt}).

\paragraph{2d-min-heap and ladders:}
Let $S=(x_{0},x_{1},\ldots,x_{n}=-\infty)$ be a
sequence on $n$ integers. Let the \emph{closest smaller successor}
of $x_{i}$ be the element $x_{j}$ such that $j = min\{j^{\prime}|
j^{\prime} > i \land x_{j^{\prime}} < x_{i}\}$. Thus, $x_{j}$ is the parent
of $x_{i}$ in the 2d-Min-Heap. The 2d-Min-Heap is then fully
determined once we find the closest smaller successor of all
elements $x_{i} \in S$.

Let $C$ be the cartesian tree of $S$. Let
the \emph{closest right ancestor} of $x_{i}$ in $C$ be the closest
ancestor $x_{j}$ of $x_{i}$ such that $x_{i}$ is in the left 
subtree of $x_{j}$. Since $x_{n} = -\infty $, both the closest
smaller sucessor and the closest right ancestor are 
well-defined for all $x_{i}$, where $0 \leq i \leq n-1$. Observe that the
closest smaller successor of $x_{i}$ and the closest right ancestor
are the same element.

Let $ET=(y_{0},y_{1},\ldots,y_{m})$ be the Euler
tour of $C$ that visits the children of each node in right-to-left
order. To ensure that each node in $C$ has two children, we add
(virtual) dummy nodes. We assume that
every node $x_i$ in $C$, and hence in $ET$, is labelled with its index
$i$ in $S$. We also assume that for some $x_i$ in $C$, we know the first
ocurrence of $x_i$ in $ET$. Both these assumption can be obtain as part of the
construction of $ET$. We can obtain the closest right ancestor by performing a list
ranking of $ET$, computing the sequence $\delta(ET) =(z_{0},
z_{1},\ldots,z_{m})$ defined as $z_{0}=y_{0}$ and 
$z_{i}=\delta(z_{i-1},y_{i})$, for all $1\leq i\leq n$, where
$\delta(\cdot,\cdot)$ is defined as

\begin{center}
        $\delta(x,y_{i}) =
             \left\{
             	\begin{array}{ll}
	        	y_{i}, & \text{if } s(i) < i, \text{where $s(i)$ denotes the index of $y_i$'s successor in $ET$}\\
		        x, & \text{otherwise}
               	\end{array}
              \right.
       $
\end{center}

See Figure \ref{fig:cartesian-b} as an example of the Euler Tour $ET$ of the
tree in Figure \ref{fig:cartesian-a} and its corresponding sequence $\delta(ET)$.

\begin{figure}[t]
    \centering
    \begin{subfigure}[c]{0.98\textwidth}
        \centering
        \includegraphics[scale=.35]{./images/cartesian-a.eps}
        \caption{Cartesian tree of the sequence
    $(1,4,9,5,10,7,3,2,5,4,-\infty)$. Dummy nodes are represented with squares.}
        \label{fig:cartesian-a}
    \end{subfigure}
    \begin{subfigure}[c]{0.98\textwidth}
    \vspace{1em}
        \centering
        \includegraphics[scale=.45]{./images/cartesian-b.eps}
        \caption{Euler Tour $ET$ of the cartesian tree in
    Figure \ref{fig:cartesian-a} and the sequence $\delta(ET)$. The first
    ocurrence of each node of the cartesian tree is highlighted with a
    square. The arrows show the closest right ancestor of each node.}
        \label{fig:cartesian-b}
    \vspace{1em}
    \end{subfigure}
    \caption[Example of the proof of
    Lemma \ref{lemma:2d-min-heap}]{Example of the proof of
    Lemma \ref{lemma:2d-min-heap}. The resulting 2d-Min-Heap corresponds to the
    tree in Figure \ref{fig:2dMinHeap-a}.}
    \label{fig:cartesian}
\end{figure}

\begin{lemma}\label{lemma:2d-min-heap}
If $y_{i}$ is the first occurrence of some element $x_{j}$ in $ET$,
then the element $z_{i-1}$ in $\delta(ET)$ is $x_{j}$'s closest right
ancestor in $C$.
\end{lemma}

\begin{proof}
Since $ET$ visits all the descendants of a node $x_{k}$ after the first
occurrence of $x_{k}$ in $ET$, the first occurrence of $x_{j}$ in $ET$
comes after the first occurrence of $x_{k}$ if $x_{k}$ is $x_{j}$'s
closest right ancestor. Now assume that $y_{h}$ is the last occurrence of $x_{k}$ before the first
occurrence $y_{i}$ of $x_{j}$. Let $(y_{h},y_{h+1},\ldots,y_{i})$ be the
subsequence of $ET$ between 
$y_{h}$ and $y_{i}$, and let $(k=j_{h},j_{h+1},\ldots,j_{i}=j)$ be the sequence
of indices such
that $y_{t} = x_{j_{t}}$, for all $h \leq t \leq i$, where $x_{j_{t}}$ is the
$j_{t}$-th element in $ET$.

To prove the lemma, we need to show that $j_{h+1} < j_{h} $ and $j_{t+1} > j_{t}$ for all
$h < t < i$ because this implies that $\delta(x, y_{h}) = y_{h} =
x_{k}$ and $\delta(x, y_{t}) = x$ for all $h < t < i$, that is, $z_{h}
= \delta(z_{h-1}, y_{h}) = x_{k}$ and $\delta(z_{t-1}, y_{t}) = z_{t-1}
= z_{h} = x_{k}$ for all $h < t < i$; in particular, $z_{i-1} =
x_{k}$, as claimed.
The node $y_{h+1}$ must be $x_{k}$'s left child in $C$ because the
last visit to $x_{k}$ by $ET$ before visiting any node in $x_{k}$'s
left subtree happens immediately before visiting $x_{k}$'s left
child. Thus, $j_{h+1} < j_{h}$. All the nodes on the path from
$y_{h+1}$ to $x_{j}$ in $C$ are left ancestors of $x_{j}$ because
$x_{k}$ is the closest right ancestor of $x_{j}$. Thus, by the
definition of $ET$,$(y_{h+1}, y_{h+2},\ldots, y_{i})$ is
the sequence of nodes in this path. Since $y_{t+1}$ is the right child
of $y_{t}$ for all $h < t < i$, we have that $j_{t+1} > j_{t}$.

See Figure \ref{fig:cartesian} as an illustration of this proof.

\end{proof}

We can parallelize this strategy using the results
of \cite{Shun:2014:SPC:2632163.2661653} to obtain the cartesian tree
$C$ of $S$ with $O(n)$ work, $O(\lg^{2}n)$ span and $O(n)$ working space, our {\tt PFEA} algorithm in
Section \ref{subsec:PFEA} to compute the Euler tour
$ET$ with $O(n)$ work, $O(\lg n)$ span and $O(n\lg n)$ working space, and the
results of \cite{Helman2001265} to compute the list 
ranking using the function $\delta(\cdot,\cdot)$ with $O(n)$ work, $O(\lg n)$
span and $O(n)$ working space.

Next we compute $\delta(ET)$ using list ranking
(Lemma \ref{lemma:2d-min-heap}). Then, we assign one core to every element
$z_{i} \in \delta(ET)$. The core writes $z_{i}$ as the closest right
ancestor of $x_{j}$ if and only if $y_{i}$ is the first occurrence of
$x_{j}$ in $ET$. This is done in $O(1)$ time.

Thus, the complexity of constructing the 2d-Min-Heap in parallel is $O(n)$ work,
$O(\lg^{2}n)$ span and $O(n\lg n)$ working space.



\begin{figure}[t]
  \centerline{\includegraphics[width=.9\textwidth]{./images/sorted2dMinHeap.eps}}
  \caption[Computation of the ladders of a tree $T^{\mathcal{B}}$, with embedding
  $\mathcal{B}$.]{Computation of the ladders of a tree $T^{\mathcal{B}}$, with embedding
  $\mathcal{B}$. The tree $T^{\mathcal{B}}$ is the result of applying the embedding $\mathcal{B}$ to the
  tree of Figure \ref{fig:2dMinHeap}. In the tree, the depth of each node is
  shown. For example, $d:3$ means that the depth of a node is $3$. In the Euler
  Tour $ET_n$, the dummy root is represented by the symbol $\bf{r}$.}
  \label{fig:sorted2dMinHeap}
\end{figure}


After constructing the 2d-Min-Heap, the tree is decomposed into ladders. The
ladders are constructed by recursively extracting the 
longest path of the tree. This gives us a set of paths.
Then, each path of length $l$ is extended by adding at most $l$
nodes towards the root. Those extended paths are
called ladders. To construct the ladders in parallel, assume that we
have a tree $T^{\mathcal{B}}$ with a particular embedding $\mathcal{B}$: for each node $v$ of
$T^{\mathcal{B}}$, the children of $v$ are ordered by their height, with the highest
child in the leftmost position.

\begin{lemma}\label{lemma:ladders}
Given a tree $T^{\mathcal{B}}$ with embedding $\mathcal{B}$ and $n$ nodes, the ladders of
$T^{\mathcal{B}}$ can be constructed in parallel with $O(n\lg n)$ work, $O(\lg n)$ span
and $O(n\lg n)$ working space.
\end{lemma}
\begin{proof}
To prove the lemma, we need to compute the depth of each node of the tree. This
can be done in parallel by using the {\tt PFEA} algorithm of Section
Section \ref{subsec:PFEA}, adding $1$ for each forward edge and subtracting $1$
for each backward edge. It takes $O(n)$ work, $O(\lg n)$ span and $O(n\lg n)$
working space. Now, let $ET_n=(v_0,\ldots,v_m)$ be the Euler tour of
$T^{\mathcal{B}}$ that visits the children of each node in left-to-right order and
writes the index of each node. Let $ET_d=(v^{\prime}_0,\ldots,v^{\prime}_m)$
be the Euler tour of $T^{\mathcal{B}}$ that visits the children of each node in
left-to-right order and writes the depth of each node. We can decompose the tree
into paths by finding contiguous increasing
subsequences in $ET_d$. By the definition of the embedding $\mathcal{B}$,
the resulting paths are the same ones obtained by recursively
extracting the longest path of the tree. For the
path represented by the subsequence $ET_d[a..b]$, $ET_d[b]$ corresponds to the
depth of the leaf of this path and the length of $ET_d[a..b]$ is $b-a+1$.

To compute the
ladders, we need to extend each subsequence in $ET_d$. For a subsequence $ET_d[a..b]$, if
$ET_n[a]$ is the root of the tree, then the subsequence does not need to be
extended. Otherwise, the subsequence need to be extended by adding up to $x=(b-a-1)$
extra nodes. If $x=0$, then the subsequence does not need to be extended. The
extra nodes that we need to add correspond to ancestors of the leaf $ET_n[b]$ at
depths $ET_d[a]-i$, $i\in(1,\ldots,x)$. We use the operation $\levelanc(ET_n[a],i)$,
$i\in(1,\ldots,x)$, of the simplified NS-representation (see
   Table \ref{tbl:operations}, operation 5) to obtain all the ancestors and
   the ladders.

The Euler Tours $ET_n$ and $ET_d$ can be found using the {\tt PFEA} algorithm. The
bounds of all increasing subsequences can be found in parallel by finding each
index $i$, such that $ET_d[i]<ET_d[i-1]$ or $ET_d[i]>ET_d[i+1]$. This can be done with
$O(n)$ work, $O(1)$ span and $O(n\lg n)$ working space. The
simplified NS-representation can be constructed with $O(n)$ work, $O(\lg n)$
span and $O(n\lg n)$ working space. The $\levelanc$ operation of the simplified
NS-representation takes $O(\lg n)$ time to be answered. Since the total length of all
the ladders is $2n$ \cite{Navarro:2014:FFS:2620785.2601073}, the amount of operations that we need to
perform is $O(n)$. We can perform all the operations independently, so the
$O(n)$ $\levelanc$ operations can be answered in parallel with $O(n\lg n)$ work
and $O(\lg n)$ span.

Figure \ref{fig:sorted2dMinHeap} shows an example of the Lemma \ref{lemma:ladders}.

\end{proof}


Alternatively, observe that we can extend a subsequence $ET_d[a..b]$ using
wavelet trees \cite{bib:grossi03, Makris12, Navarro_2012}. Since the extra nodes that we need to add correspond to the ancestors
of the leaf $ET_n[b]$, they appear before $ET_n[b]$ in the Euler Tours, with depths
$ET_d[a]-i$, $i\in(1,\ldots,x)$. Given a node $v$ at position $j$ in $ET_d$, we
know that the parent of $v$ is at
position $k$ in $ET_d$, where $k=max\{k'|k'<j, ET_d[k']=ET_d[j]-1\}$. In general,
to extend the subsequence $ET_d[a..b]$, we need the nodes at positions
$max\{k'|k'<a, ET_d[k']=ET_d[a]-i\}$, with $i\in(1,\ldots,x)$. Those positions can be
found by using rank/select operations over $ET_d$. To answer the
rank/select operations efficiently, we could construct a wavelet tree over $ET_d$, considering the
contiguous alphabet $\Sigma=\{0,\lceil \lg |ET_d|\rceil-1\}$. For example, to
extend the subsequence $ET_d[a..b]$ with a node with depth $d'$, we need to
perform $\selop_{d'}(ET_d,\rankop_{d'}(ET_d,a))$. Finally, once we find the
position of all the nodes, we use $ET_n$ to obtain their indexes. To construct
the wavelet trees in parallel, we can use Algorithm {\tt PWT} or
algorithm {\tt DD} of \cite{Ferres2015}. With Algorithm {\tt PWT}, the wavelet
tree can be computed with $O(n\lg n)$ work, $O(n)$ span and $O(n\lg n)$ working
space; with Algorithm {\tt DD}, the wavelet tree can be computed with
$O(n\lg n)$ work, $O(\lg n)$ span and $O(n^2\lg n)$ working space. If there are
$p<\lg n$ available threads, the working space of Algorithm {\tt DD} is reduced
to $O(n\lg^2 n)$. The rank/select operations over the 
wavelet tree take $O(\lg n)$. We can perform all the operations independently,
so the $O(n)$ rank/select operations can be answered in parallel in $O(\lg n)$
time.

The following lemma presents an extension of the Lemma \ref{lemma:ladders} for
trees with arbitrary embeddings.

\begin{lemma}\label{lemma:tree-embedding}
Given a tree $T^{\mathcal{A}}$ with an arbitrary embedding $\mathcal{A}$ and $n$ nodes, the ladders of $T^{\mathcal{A}}$ can
be constructed in parallel with $O(n\lg n)$ work, $O(\lg n)$ span and $O(n\lg n)$
working space.
\end{lemma}

\begin{proof}
To prove this lemma, we need to map the embedding $\mathcal{A}$ of $T^{\mathcal{A}}$ to $\mathcal{B}$. To
compute the embedding $\mathcal{B}$, we need to order all the children of the nodes
of $T^{\mathcal{A}}$ by height with the highest in the leftmost position. To do this, we use the {\tt
PFEA} algorithm to compute the folklore encoding of $T$ and then construct its
simplified NS-representation to use the $\height$ operation to obtain the
height of all the nodes. Both the folklore enconding and the simplified
NS-representation can be computed with $O(n)$ work, $O(\lg n)$
span and $O(n\lg n)$ working space. The $\height$ operation takes $O(\lg n)$ and
there are $n$ operations, and therefore all the operations can be done with
$O(n\lg n)$ work and $O(\lg n)$ span. After that, we can use a parallel stable
sorting algorithm over the children of the nodes of $T^{\mathcal{A}}$. Raman \cite{Raman1990Sorting} sorts an array
of $n$ integers each in the domain $[1,\ldots,m]$, for $m=n^{O(1)}$, with
$O(n\lg\lg m)$ work, $O(\lg n/\lg\lg n+\lg\lg m)$ span and $O(n\lg m)$ working
space. In our 
case, the total number of children in $T$ is $n-1$ or $2(n-1)$ by using bidirectional
edges, and the height of any node is less than $n$. Therefore, we can sort the children
of all the nodes of $T$ with $O(n\lg\lg n)$ work, $O(\lg n/\lg\lg n)$ span and
$O(n\lg n)$ space.

With the new embedding $\mathcal{B}$, we use Lemma \ref{lemma:ladders} to finish
the proof.

In the NS-representation, the
2d-Min-Heap has $\tau$ nodes, and therefore, the 2d-Min-Heap and its ladders can
be computed with $O(\tau\lg\tau)$ work, $O(\lg^2\tau)$ span and $O(\tau\lg\tau)$
working space.
\end{proof}

Considering the results of Bender and Farach-Colton to solve the {\em level
ancestor problem} \cite{BENDER20045}, we can use
Lemma \ref{lemma:tree-embedding} to 
parallelize their solution. Their solution is based on the computation of
ladders, pointers to ancestors and lookup tables of a rooted tree. Ladders can
be computed using Lemma \ref{lemma:tree-embedding}. The pointers and lookup
tables can be computed by traversing the tree, using the parallel Euler Tour
algorithm introduced in Algorithm \ref{algo:PFEA}.


\paragraph{P\v{a}tra\c{s}cu's bitmap:} Navarro and Sadakane use the sparse bitmap
of P\v{a}tra\c{s}cu \cite{Patrascu:2008:SUC:1470582.1470670} to represent a
bitmap with $2\tau$ $1$'s and $2\tau w^c$ $0$'s using $O(\tau\lg w^{c} + \frac{\tau
  w^{c}t^{t}}{\lg^{t}(\tau w^{c})}+(\tau w^{c})^{3/4})$ bits and supporting
  rank/select queries in $O(t)$ time. P\v{a}tra\c{s}cu demostrated how to use
  recursion to achieve a better redundancy. Given a sparse
  bitmap $A$ of size $n$, the succinct representation of $A$ is constructed as
  follows: Choose $B\geq 2$ such that $B\lg B =  \frac{\varepsilon\lg n}{t}$,
  and $r=B^{t}=(\frac{\lg n}{t})^{\Theta(t)}$. We first divide the bitmap $A$ into 
$n/r$ segments of size $r$. Each segment is stored in a \emph{succinct
  aB-tree}. Each succinct aB-tree is constructed by dividing the bitmap into $B$
independent segments. On each small segment, the author applies Lemma 3 of \cite{Patrascu:2008:SUC:1470582.1470670}
recursively $t$ times. In order to reduce the redundancy, on each application of the lemma, $M$ memory bits are
extracted from the values of the independent segmens and stored, and the rest of
the unextracted bits, called {\em spill}, are passed to the next iteration. Then, Lemma 5
of \cite{Patrascu:2008:SUC:1470582.1470670} is applied
 in each succinct aB-tree, storing the last spill and memory
 bits in the root of each aB-tree. For each segment of size $r$, the
 index in memory of the segment's memory bits are
 stored. Additionally, the number of ones of each segment are stored
 in a partial sums vector and a predecessor structure to support rank
 and select operations, respectively.

The parallel algorithm to construct the P\v{a}tra\c{s}cu's bitmap is
similar to the parallel algorithm we used to construct the {\tt RMMT}
in Section \ref{subsec:PSTA}. First, we construct the
$n/r$ succinct aB-trees in parallel. On each aB-tree, we divide the bitmap on $B$
independent bitmaps of size $r/B$, similar to the {\tt PSTA} algorithm. We apply Lemma 3
of \cite{Patrascu:2008:SUC:1470582.1470670} recursively on each small bitmap, $t$
times. Then, we apply Lemma 5 of
\cite{Patrascu:2008:SUC:1470582.1470670} in each succinct aB-tree,
storing the final \emph{spill} and memory bits in the root of each
aB-tree. After that, all the $n/r$ succinct aB-trees are built with $O(n)$ work
and $O(t)$ span. The next step consists of storing the values of the roots of 
each aB-tree. To support the rank operation, we compute in parallel the partial sum vector of
these values with $O(n/r)$ work and $O(\lg(n/r))$ span using a parallel prefix sum
algorithm. To support select operation, we can use a fusion
tree. Below, we will explain how to construct a fusion tree in parallel
with $O(n/r)$ work and $O(\lg\lg(n/r))$ time. Finally, P\v{a}tra\c{s}cu's
sparse bitmap can be computed in parallel, with $r=(\frac{\lg
n}{t})^{\Theta(t)}$, in $O(n+\frac{nt^t}{\lg^t n})$ work, $O(t
+ \lg(\frac{nt^t}{\lg^t n}))$ span and $O(n)$ working space. In the context of
succinct trees, the work is $O(\tau w^c+\frac{\tau w^ct^t}{\lg^t \tau w^c})$,
the span is $O(t + \lg(\frac{\tau w^ct^t}{\lg^t (\tau w^c)}))$ and the working space is
$O(\tau w^c)$.

\paragraph{Fusion tree:}
A fusion tree stores an array $A$ of size $n$ of $w$-bit integers, supporting
predecessor/successor queries in $O(\lg_w n)$ time. A fusion tree
is essencially a B-tree with branching factor $w^{1/5}$, and therefore, if we can
construct a B-tree over the array $A$ in parallel, we also obtain a parallel algorithm to construct fusion
trees. In \cite{BiingFeng199455}, Wang and Chen
present a parallel algorithm to construct B-trees in $O(\lg\lg n)$ time, for a
sorted list. Since the $\lg\tau$ values of  
the sequence of accumulated weights used to answer $\fwdsearch$
queries are always increasing, we can apply the algorithm described
in \cite{BiingFeng199455} to construct the B-tree. Henceforth, we will consider
the array $A$ as a sorted list of $n$ keys.
Although the algorithm of Wang and Chen is based on the 
EREW model, it can be applied in SMP systems without any modifications. If there are
$p$ available cores, the complexity of the algorithm is
$O(n/p)$. 

Given the sorted list $A$, the algorithm of Wang and Chen constructs an uniquely
defined B-tree with branching factor $m$ and the following properties:
\begin{itemize}
   \item The B-tree has the minimal height $h=\lceil\lg_m(n+1)\rceil+1$
   \item The root owns $\lceil(n+1)/m^h-1\rceil$ keys
   \item There exists an integer $c$, $1<c\leq h+1$, such that all the nodes of
   the B-tree above the $c$-th level contain $m-1$ keys and all the non-leaf node
   below the $c$-th level contain $\lceil m/2\rceil-1$ keys.
   \item The leftmost leaf of the B-tree contains $s$ keys,
   $\lceil m/2\rceil\leq s\leq m-1$. The rest of the leaves may contain $s$ or $s-1$
   keys, but may not own more keys than the leaf node on its left.
\end{itemize}

With this well-defined B-tree, the parallel algorithm computes the position of
each key of $A$ in the B-tree. Each node of the B-tree is identified by its
order in a BFS traversal. The details of how to assign a position to each key
of $A$ are shown in \cite{BiingFeng199455}.

Once we have the B-tree, we apply the \emph{sketch}
algorithm \cite{Fredman1993424} in parallel on each node of the tree, in
$O(1)$ time. Hence, the fusion tree can be computed with $O(n)$ work, $O(\lg\lg n)$
span and $O(n)$ working space.

In the NS-representation, to support $\fwdsearch$ and $\bwdsearch$ operations,
$\tau$ fusion trees are constructed over $\tau$ sorted arrays of $O(\lg\tau)$
integers. Considering the previous parallel bounds, the $\tau$ fusion trees can
be constructed with $O(\tau\lg\tau)$ work, $O(\lg\lg\tau)$ span and
$O(\tau\lg\tau)$ working space.

\paragraph{Range-minimum-query:}
In \cite{Fischer:2011:SPS:2078866.2078875}, Fisher and Heun present a
data structure to answer range minimum/maximum queries in
constant-time using $O(n)$ bits over an array $A$ of $n$ elements.
The array $A$ is preprocessed by dividing it into $\lceil n/s\rceil$ blocks,
$B_1,\ldots,B_{\lceil n/s\rceil}$, of size $s=\lceil \frac{\lg n}{4}\rceil$. A
query from $i$ to $j$, $\RMQ(A,i,j)$, is divided into at most three subqueries:
One {\em in-block} query over the block $B_{\lfloor i/s\rfloor}$, one {\em
out-of-block} query over the blocks $B_{\lceil i/s\rceil},\ldots,B_{\lfloor
j/s\rfloor-1}$ and one {\em in-block} query over the block $B_{\lfloor
j/s\rfloor}$. If $i$ and $j$ belong to the same block, then only one in-block
query it is necessary. The in-block queries allow us to obtain the
minimum/maximum element inside a block. On the other hand, out-of-block queries
allow us to obtain a minimum/maximum element from consecutive blocks.

To answer in-block queries, authors use the fact that each block $B_x$ can be
represented by an unique {\em canonical cartesian tree} $C^{can}_{B_x}$. The
canonical cartesian tree of $B_x$ is a cartesian tree with a total order $\prec$
defined as follows: $B_x[i]\prec B_x[j]$ iff $B_x[i] < B_x[j]$, or $B_x[i] =
B_x[j]$ and $i<j$. The idea is to precompute all the answers for all $C_s$
possible canonical cartesian trees, where $C_s = \frac{1}{s+1}{2s\choose s}$ is
the number of the rooted trees on $s$ nodes. Thus, all the answers are stored in
a table $P[1,C_s][1,s][1,s]$. The first dimension of the table $P$ corresponds
to a descriptor of the blocks of size $s$. For all $\lceil n/s\rceil$ blocks of
$A$, their descriptors are stored in an array $T$, requiring $O(s)$ time to
compute each one \cite{Fischer:2011:SPS:2078866.2078875}.

To answer out-of-block queries, the minimum/maximum element of each block is
stored in an array $A^{\prime}[1,n^{\prime}]$, where $n^{\prime}=\lceil
n/s\rceil$. The array $A^{\prime}$ is divided into $\lceil n^{\prime}/s\rceil$
blocks, $B^{\prime}_1,\ldots,B^{\prime}_{\lceil n^{\prime}/s\rceil}$. A \RMQ{}
query over $A^{\prime}$ is answered as before: one out-of-block query and two
in-block queries. The in-block queries can be answered by computing the
descriptor of each block of $A^{\prime}$, storing them in an array $T^{\prime}$
and reusing the lookup table $P$. To answer the out-of-block queries of
$A^{\prime}$, a 
two-level storage scheme is used. $s$ contiguous blocks of $A^{\prime}$ are
grouped into a {\em superblock} consisting of $s^{\prime}=s^2$ elements. We
precompute all the answers in $A^{\prime}$ that cover at least one such
superblock and store them into a table $M$. Similarly, we precompute all the
answers in $A^{\prime}$ that cover at least one block, but not over a superblock
and store them into a table $M^{\prime}$, Thus, to find the minimum/maximum element
inside a superblock, we need to use the table $M$ twice. Summarizing, an
out-of-block query of $A$ can be decomposed into two in-block queries in
$A^{\prime}$ (using $T^{\prime}$ 
and $P$), two out-of-block queries in $A^{\prime}$ (using $M^{\prime})$ and one
out-of-superblock query in $A^{\prime}$ (using $M$).

Finally, the solution of Fisher and Heun has $O(n)$ construction time,
$O(\lg^{3}n)$ construction space (over the $O(n)$ space of the structure) and
$O(1)$ query time.

Since the minimum/maximum operation is associative, we can use a domain
decomposition strategy to parallelize the construction of the solution of Fisher
and Heun. Thus, we can obtain a parallel solution with $T_1=O(n)$ work,
$T_{\infty}= O(\lg n)$ span and the same space complexity. The term $O(\lg n)$
is due to the traversal of the blocks of size $s=\lceil \frac{\lg n}{4}\rceil$,
which is done sequentially.

In the context NS-representation, we need to answer queries over the root of
the $\tau$ {\tt RMMT}s. Therefore, to answer range minimum/maximum queries we can
construct the solution in \cite{Fischer:2011:SPS:2078866.2078875} with $O(\tau)$
work, $O(\lg \tau)$ span and $O(\tau+\lg^{3}\tau)$ working space.

\paragraph{Degree, Child and Childrank operations:} To support $\degreeop$,
$\child$ and $\childrank$, we need to compute marked blocks. Remember that
pioneers are the tighest matching pairs of parentheses 
$(i,j)$, with $j=\findclose(i)$, such that $i$ and $j$ belong to
different blocks. A marked block is a block that has the opening
parenthesis of a pionner $(i,j)$ such $i$ and $j$ do not belong to consecutive
blocks. To compute such marked
blocks, we need to apply the $\findclose$ operation over all the $\tau$
blocks. Since the $\findclose$ operation can be computed in constant time, all
marked blocks can be computed with $O(\tau)$ work and $O(1)$ span. The $\child$
and $\childrank$ operations additionally need to construct a sparse bitmap $C$
for each marked block, which encodes the number of children of the marked block,
in left-to-right order, as gaps of $0$'s between $1$'s. Therefore, to construct
each bitmap it is enough to find the position of each $1$ in the bitmap. To do
so, we perform a parallel prefix sum over the blocks fully contained in a marked
block. Let $j$ be a marked block. The bitmap $C_j$ of $j$ can be constructed as
follows: for each block $j'$ fully contained in $j$, if $j'$ has at least one child of
$j$, we obtain the number of children of $j'$. If $j'$ is not a marked block, then the
number of children corresponds to $n_{j'}$ (the number of minima of block $j'$);
if $j'$ is a marked node, the number of children is $1$. Notice that if $j'$ is
marked, then the blocks contained in $j'$ do not have any children of $j$ and they
will not be considered for the rest of the computation of $C_j$. After that, we
perform a parallel prefix sum over the blocks that do have some children of $j$,
considering their left-to-right order. The result of the prefix sum corresponds
to the position of all the $1$'s in $C_j$. The final step is to write, in
parallel, all the $1$'s where they correspond. Following the same idea, we can
compute a bitmap $C$ that represents the concatenation of all the bitmaps of the
marked nodes. The parallel prefix sum is the most expensive step of this
algorithm, and its work is $O(\tau)$, its span is $O(\lg\tau)$ and its
working space is $O(\tau\lg w^c)$ bits, which is dominated by the array of size
$O(\tau)$ used in the prefix sum, where each element uses $O(\lg w^c)$ bits.


Thus, with $w=\Theta(\lg n)$ we have the following theorem:

\begin{theorem}\label{lem:lg-O(1)}
A $(2n+O(n/\lg^{c} n))$-bit representation of an ordinal tree on $n$ nodes and its
balanced parenthesis sequence can be computed with
$O(n+\frac{n}{\lg^{c}n}\lg(\frac{n}{\lg^c n})+c^c)$ work,
$O(c+\lg(\frac{nc^c}{\lg^cn}))$ span and $O(n\lg n)$ bits of working space. This
representation supports the operations in Table \ref{tbl:operations} 
in $O(c)$ time, with $c>3/2$.
\end{theorem}
\begin{proof}
Each of the $\tau$ {\tt RMMT}s can be constructed with $O(\lg^c n)$ work, $O(\lg\lg^c n)$
span and $O(\lg^c n\lg\lg^c n)$ bits of working space using Theorem \ref{lem:lg-O(1)}. All
the $\tau$ {\tt RMMT}s can be constructed with $O(n)$ work, $O(\lg\lg^c n)$
span and $O(n\lg n)$ working space. Using the results of this section, with $t=c$, the
additional data structures can be constructred with
$O(n+\frac{n}{\lg^{c}n}\lg(\frac{n}{\lg^c n})+c^c)$ work,
$O(c+\lg(\frac{nc^c}{\lg^cn}))$ span and $O(n+\frac{n}{\lg^{c-1}n})$ working
space. Thus, total work is $O(n+\frac{n}{\lg^{c}n}\lg(\frac{n}{\lg^c n})+c^c)$,
the maximun span is $O(c+\lg(\frac{nc^c}{\lg^cn}))$ and the total working space
is $O(n\lg n)$ bits.
\end{proof}
