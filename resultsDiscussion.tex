\subsection{Experimental results of the PSTA algorithm}
\label{subsec:expsPSTA}

To evaluate the performance of our {\tt PSTA} algorithm, we compare it against
{\tt libcds}~\cite{libcds} and {\tt sdsl}~\cite{sdsl}, which are
state-of-the-art implementations of the {\tt RMMT}.  Both assume that the input
tree is given as a parenthesis sequence, as we do here.  Our implementation of
the {\tt PSTA} algorithm deviates from the description in
Section~\ref{subsec:PSTA} in that we do not store the array $n'$, since {\tt
libcds} and {\tt sdsl} do not store it and that the prefix sum computation in
line~22 of the algorithm is done sequentially in our implementation.  This
changes the running time to $O(n/p + p)$ but simplifies the implementation.
Since $p \ll n/p$ for the input sizes we are interested in and the numbers of
cores available on current multicore systems, the impact on the running time is
insignificant. In our experiments, the chunk size $s$ was fixed at 256.


\subsubsection{Running time and speed-up}

Table~\ref{tbl:parallelTimes} shows the wall clock times achieved by
{\tt psta}, the sequential version of {\tt psta}, called {\tt seq}, {\tt libcds},
and {\tt sdsl} on different inputs.
Each time is the median achieved over five non-consecutive runs, reflecting
our assumption that slightly increased running times are the result of
``noise'' from external processes such as operating system and networking tasks.
Figure~\ref{fig:speedup-seq} shows the speed-up compared to the running times of {\tt seq}, and
Figure~\ref{fig:speedup-sdsl} shows the speed-up compared to {\tt sdsl}.

The differences in running times of the {\tt psta} algorithm on one core and
{\tt seq} are insignificant. This implies that the overhead of the scheduler is
negligible. The {\tt psta} algorithm on a single core and {\tt sdsl} outperformed
{\tt libcds} by an order of magnitude.  One of the reasons for this is
that {\tt libcds} implements a different version of {\tt RMMT}
including {\em rank} and {\em select} structures, while {\tt psta} and
{\tt sdsl} do not.  Constructing these structures is costly.  On a
single core, {\tt sdsl} was about 1.5 times faster than {\tt psta},
but neither {\tt sdsl} nor {\tt libcds} were able to take advantage of
multiple cores, so {\tt psta} outperformed both of them starting at
$p = 2$.  The advantage of {\tt sdsl} over {\tt psta} on a single
core, in spite of implementing essentially the same algorithm, can be
attributed to the lack of tuning of {\tt psta}.

\begin{figure}[t]
  \begin{minipage}[b]{0.47\textwidth}
    \setlength{\tabcolsep}{0pt}
    \begin{tabular}{c@{\hspace{1em}}r@{ }r@{ }r@{ }r@{ }r}
      \toprule
      $p$ & {\tt wiki} & {\tt prot} & {\tt dna} & {\tt ctree} & {\tt osm}\\
      \midrule
          {\tt libcds} & 33.17 & 44.27 & 75.93 & 140.71 & 339.43\\
          {\tt sdsl} & 1.94 & 2.67 & 4.57 & 8.35 & 18.10\\
          {\tt seq} & 2.81 & 4.10 & 7.25 & 12.14 & 28.00\\
          \midrule
          {\tt seq} & 2.81 & 4.10 & 7.25 & 12.14 & 28.00\\
          1 & 2.81 & 4.10 & 7.15 & 12.17 & 28.05\\
          4 &.72 & 1.05 & 1.86 & 3.05 & 7.07\\
          8 &.40 &.58 &.95 & 1.57 & 3.55\\
          12 &.31 &.43 &.72 & 1.12 & 2.55\\
          16 &.24 &.32 &.55 &.85 & 1.89\\
          20 &.19 &.29 &.49 &.74 & 1.58\\
          24 &.19 &.26 &.42 &.68 & 1.45\\
          28 &.16 &.25 &.43 &.62 & 1.30\\
          32 &.18 &.25 &.38 &.62 & 1.16\\
          36 &.20 &.21 &.36 &.52 & 1.08\\
          40 &.21 &.23 &.35 &.50 & 1.04\\
          44 &.22 &.25 &.34 &.51 &.97\\
          48 &.21 &.26 &.37 &.49 &.99\\
          52 &.27 &.30 &.36 &.50 &.93\\
          56 &.30 &.36 &.42 &.50 &.93\\
          60 &.27 &.40 &.39 &.50 &.93\\
          64 &.30 &.33 &.38 &.54 &.90\\
      \bottomrule
    \end{tabular}
  \end{minipage}%
  \hspace{\stretch{1}}%
  \begin{minipage}[c]{0.5\textwidth}
%    \leavevmode\rlap{\includegraphics[width=1.04\textwidth]{./images/speedup}}
    \includegraphics[width=1.04\textwidth]{./images/speedup-psta}
    \vspace{.2ex}
    \captionof{figure}{Speed-up of {\tt PSTA} compared to {\tt seq}.\label{fig:speedup-seq}}
    \vspace{1ex}
    \includegraphics[width=1.04\textwidth]{./images/speedup-sdsl}
  \end{minipage}\\[1ex]
  \leavevmode\begin{minipage}[t]{0.47\textwidth}
    \captionof{table}{Running times of {\tt libcds}, {\tt sdsl}, and {\tt PSTA},
      in seconds. {\tt seq} corresponds to the sequential execution of {\tt PSTA}.\label{tbl:parallelTimes}}
  \end{minipage}%
  \hspace{\stretch{1}}%
  \begin{minipage}[t]{0.5\textwidth}
    \captionof{figure}{Speed-up of {\tt PSTA} compared to {\tt sdsl}.\label{fig:speedup-sdsl}}
  \end{minipage}
\end{figure}

Up to 16 cores, the speed-up of {\tt psta} with {\tt ctree} and {\tt osm}
datasets is almost linear whenever $p$ is a
power of $2$ and the efficiency (speed-up/$p$) is 70\% or higher with respect to
{\tt seq} and 60\% with respect to {\tt sdsl}, except for {\tt ctree} on 32 cores.
This is very good for a multicore architecture.
When $p$ is not a power of~$2$, speed-up is slightly worse.
The reason is that, when $p$ is a power of $2$, {\tt psta} can assign exactly
one subtree to each thread (see Algorithm \ref{algo:PSTA2}), distributing the
work homogeneously across cores without any work stealing.
When the number of threads is not a power of two, some threads have to process
more than one subtree and other threads process only one, which degrades
performance due to the overhead of work stealing.

There were three other factors that limited the performance of {\tt psta} in
our experiments: input size and resource contention withthe OS.

\paragraph{Input size}
For the two largest inputs we tested, {\tt osm} and {\tt ctree}, speed-up
kept increasing as we added more cores.
For {\tt wiki}, {\tt prot} and {\tt dna}, however, the best speed-up were
achieved with 28, 36 and 44 cores, respectively.
Beyond this, the amount of work to be done per thread was small enough that
the scheduling overhead caused by additional threads started to outweigh the
benefit of reducing the processing time per thread further.

\paragraph{Resource contention}
For $p < 64$, at least one core on our machine was available to OS processes,
which allowed the remaining cores to be used exclusively by {\tt psta}.
For $p = 64$, {\tt psta} competed with the OS for available cores.
This had a detrimental effect on the efficiency of {\tt psta} for $p = 64$.

The network topology of our machine {\em may} also impact in the performance of
our algorithm. The four processors on our machine were connected in a grid
topology~\cite{Drepper2007}. Up to 32 threads, all threads can be run on a
single processor or on two adjacent processors in the grid, which keeps the cost
of communication between threads low. Beyond 32 threads, at least three
processors are needed and 
at least two of them are not adjacent in the grid. This may increases the cost
of communication between threads on these processors. In Section
\ref{subsec:discussion}, we will discuss about the relationship of the topology
of multicore machines and the performance of parallel algorithms.

\subsection{Memory usage}

\begin{figure}
  \centering
  \includegraphics[scale=.4]{./images/memory}
  \caption{Memory consumption of the algorithms {\tt psta}, {\tt libcds} and {\tt sdsl}.}
  \label{fig:memory}
\end{figure}

We measured the amount of working memory (i.e., memory not occupied by the raw
parenthesis sequence) used by {\tt psta}, {\tt libcds}, and {\tt sdsl}.
We did this by monitoring how much memory was allocated/released with
\texttt{malloc}/\texttt{free} and recording the peak usage.
For {\tt psta}, we only measured the memory usage for $p = 1$.
The extra memory needed for thread scheduling when $p > 1$ was negligible. The
results are shown in the Figure \ref{fig:memory}.
Even though {\tt psta} uses more memory than both {\tt libcds} and {\tt sdsl},
the difference between {\tt psta} and {\tt sdsl} is a factor of less than 1.3.
The difference between {\tt psta} and {\tt libcds} is no more than a factor of
three and is outweighed by the substantially worse performance of {\tt
  libcds}. The reduced working space used by {\tt libcds} is due to the fact
  that its
implementation does not store the array of excess values. Instead, {\tt libcds}
stores rank/select structures over the input bit vector $P$, computing excess
values with $excess(i) \asgn 2\times rank_{1}(P,i)-i$, where $rank_{1}(P,i)$
gives the number of $1$s on $P$ up to the index $i$.
Part of the higher memory usage of {\tt psta} stems from the allocation of
$e^{\prime}$, $m^{\prime}$ and $M^{\prime}$ arrays which store the
partial excess values in the algorithm.
Storing these values, however, is a key factor that helps {\tt psta} achieves
very good performance. The space used by our algorithm can be reduced by
  storing local excess values in the array $e'$, instead of global
  values. However, reducing the space in such way will complicate the
  implementation of the queries over the {\tt RMMT}.


\subsection{Experimental Results of the PFEA algorithm}
\label{subsec:exps-ST-PFEA}

\begin{table}[t]
     \centering
        \setlength{\tabcolsep}{5pt}
       \small
    \begin{tabular}{crrrrrrrrr}
      \toprule
      $p$ & {\tt ctree25} & {\tt prot} & {\tt dna} & {\tt ctree25}$^{+16}$ &
        {\tt prot}$^{+16}$ & {\tt dna}$^{+16}$ & {\tt ctree25}$^{+32}$ &
        {\tt prot}$^{+32}$ & {\tt dna}$^{+32}$  \\
     \cmidrule(lr){2-4}
     \cmidrule(lr){5-7}
     \cmidrule(lr){8-10}
          {\tt seq} & 5.67 & 52.87 & 31.33 & 55.04 & 473.92 & 275.06 & 102.94 & 887.61 & 514.73\\
          \midrule
          1 & 6.07 & 34.83 & 57.29 & 54.99 & 275.23 & 474.07 & 102.80 & 514.09 & 886.01\\
          4 & 1.68 & 9.35 & 15.62 & 14.22 & 70.84 & 121.92 & 26.15 & 130.30 & 224.86\\
          8 & 1.05 & 5.77 & 9.80 & 7.33 & 36.39 & 62.79 & 13.32 & 66.35 & 114.42\\
          12 & 0.87 & 3.99 & 6.90 & 5.02 & 25.18 & 43.23 & 8.99 & 45.02 & 77.49\\
          16 & 0.73 & 3.66 & 5.57 & 3.91 & 19.59 & 33.50 & 6.86 & 34.37 & 59.15\\
          20 & 0.76 & 3.42 & 5.53 & 3.21 & 16.06 & 27.61 & 5.58 & 28.16 & 48.26\\
          24 & 0.76 & 3.41 & 5.25 & 2.77 & 13.91 & 24.03 & 4.73 & 23.96 & 41.04\\
          28 & 0.66 & 3.28 & 5.36 & 2.47 & 12.40 & 21.23 & 4.16 & 20.87 & 36.05\\
          32 & 0.67 & 3.37 & 5.71 & 2.30 & 12.52 & 19.49 & 4.08 & 18.62 & 35.22\\
          36 & 0.68 & 3.23 & 5.57 & 2.27 & 11.61 & 19.95 & 3.71 & 18.70 & 32.29\\
          40 & 0.68 & 3.15 & 5.48 & 2.16 & 10.91 & 18.75 & 3.38 & 17.17 & 29.59\\
          44 & 0.68 & 3.16 & 5.44 & 2.04 & 10.20 & 17.70 & 3.19 & 15.98 & 27.56\\
          48 & 0.69 & \bf{2.97} & 5.19 & 1.92 & 9.67 & 16.79 & 3.00 & 15.02 & 25.82\\
          52 & 0.64 & 3.05 & 5.37 & 1.84 & 9.23 & 16.01 & 2.83 & 14.12 & 24.33\\
          56 & 0.65 & 3.01 & 5.33 & 1.79 & 8.83 & 15.18 & 2.71 & 13.37 & 22.87\\
          60 & \bf{0.61} & 3.19 & 5.35 & \bf{1.72} & 8.46 & \bf{14.75} & 2.58 & 12.63 & 21.82\\
          64 & 0.71 & 3.05 & \bf{5.18} & 1.80 & \bf{8.29} & 15.13 & \bf{2.55} & \bf{12.32} & \bf{20.90}\\
      \bottomrule
    \end{tabular}
    \caption[Running times of {\tt PFEA} algorithm]{Running times of {\tt PFEA} algorithm, in seconds. {\tt seq}
        corresponds to the sequential execution of {\tt PFEA}. Columns with the
        superscript $+16$ and $+32$ represent the running times of {\tt PFEA}
        algorithm by artificially increasing the workload with 16 and 32 {\tt
        CAS} operations per edge, respectively. The best parallel times are
        shown using bold typeface.}
    \label{tbl:parallelTimes-pfea}
\end{table}

Table \ref{tbl:parallelTimes-pfea} shows the running times of the {\tt PFEA}
algorithm with the datasets {\tt ctree25}, {\tt prot} and {\tt dna}. To compute
the speedups, we used times obtained by {\tt seq}. The best parallel times are
identified using a bold typeface.

Figure \ref{fig:speedup-pfea} shows the corresponding speedup of the {\tt PFEA}
algorithm. Up to 16 threads, the speedup is almost linear, obtaining at least
49\% of efficiency (speedup/$p$) for the {\tt ctree25} dataset, that is, our
algorithm reaches at least 49\% of the linear speedup (the ideal). With more
than 16 threads, the performance of our algorithm is poor, reaching at most
16\% of efficiency for the {\tt prot} algorithm and 64 threads. The poor
efficiency of our algorithm is not explained by the DYM model. We think that it
can be explained by its low workload. Algorithms
with a low workload do not scale properly since the workload of their parallel
tasks is not enough to pay the overhead of thread scheduling and memory
transfers. In the case of the {\tt PFEA} algorithm, each edge takes part of only
a few comparisons and assignments. Therefore, the workload for each parallel 
task is not enough to take advantage of the 64 threads, even when we create
$\Theta(p)$ parallel tasks. To demostrate that the low workload is the reason
of the low efficiency, we increased artificially the workload of our
implementation. Between the lines 5 and 9, we added 16 and 32 {\tt CAS} operations. On
each iteration of the loop of line 5, each {\tt CAS} operation was executed over
$ET[i]$, increasing the workload for each edge. The complexity and correctness
of our algorithm do not change with the addition of these extra
operations. Columns 5--10 of Table \ref{tbl:parallelTimes-pfea} show the
resulting running times after adding 16 and 32 extra operations. Figures \ref{fig:speedup-pfea-16extra}
and \ref{fig:speedup-pfea-32extra} show the corresponding speedup. With 16 extra
operations, the efficiency was at least 48\% for the {\tt ctree25} dataset and 64
threads. For 16 threads, the efficiency increased, reaching a 88\% for the {\tt
ctree25} dataset. With 32 extra operations, the efficiency was at least 63\% up
to 64 threads and 94\% up to 16 threads. 


Another factor that, we think, limited the performance of the {\tt PFEA}
algorithm was the topology of the experiment. As was mentioned before, our
machine has four processors connected in a grid topology, which involves
communication costs among processors. Each processor executes up to 16
threads. We observe that in the Figure \ref{fig:speedup-pfea}, the
algorithm scales up to 16 threads. With more threads, the comunication costs may
affect the scalability. With more workload,
Figure \ref{fig:speedup-pfea-16extra} shows a linear scalability up to 32
threads. After 32 threads, the efficiency of the algorithm decreases. For the
experiment with 32 extra operations, Figure \ref{fig:speedup-pfea-32extra} shows
a similar behavior, with the difference that after 32 threads, the efficiency is
better than in Figure \ref{fig:speedup-pfea-16extra}. For 64 threads, all the
speedups have a slowdown, since the {\tt PFEA} algorithm has to compete with the
OS for the available cores. In Section \ref{subsec:discussion} we will discuss
more about the effects of the machine topology in the performance of the {\tt
PFEA} algorithm.

\begin{figure}[t]
\centering
 \begin{minipage}[c]{0.48\textwidth}
  \includegraphics[width=\linewidth]{./images/speedup-pfea.eps}
 \end{minipage}%
 \hspace{\stretch{1}}%
 \begin{minipage}[c]{0.48\textwidth}
  \includegraphics[width=\linewidth]{./images/speedup-pfea-16extra.eps}
 \end{minipage}\\[1ex]
 \leavevmode\begin{minipage}[t]{0.48\textwidth}
   \captionof{figure}[Speedup of the {\tt PFEA} algorithm with datasets {\tt
  ctree25}, {\tt dna} and {\tt prot}]{Speedup of the {\tt PFEA} algorithm with datasets {\tt
  ctree25}, {\tt dna} and {\tt prot}.\label{fig:speedup-pfea}} 
 \end{minipage}%
 \hspace{\stretch{1}}%
 \begin{minipage}[t]{0.48\textwidth}
   \captionof{figure}[Speedup of the {\tt PFEA} algorithm with datasets {\tt
  ctree25}, {\tt dna} and {\tt prot}, artificially increasing the workload with 16 {\tt CAS}
  operations per edge]{Speedup of the {\tt PFEA} algorithm with datasets {\tt
  ctree25}, {\tt dna} and {\tt prot}, artificially increasing the workload with 16 {\tt CAS}
  operations per edge.\label{fig:speedup-pfea-16extra}} 
 \end{minipage}

 \begin{minipage}[c]{0.48\textwidth}
 \centering
  \includegraphics[width=\linewidth]{./images/speedup-pfea-32extra.eps}
   \caption{Speedup of the {\tt PFEA} algorithm with datasets {\tt
  ctree25}, {\tt dna} and {\tt prot}, artificially increasing the workload with 32 {\tt CAS}
  operations per edge.} \label{fig:speedup-pfea-32extra}
 \end{minipage}%

\end{figure}

\subsection{Discussion}
\label{subsec:discussion}
For domains where trees have billions of nodes, the {\tt psta} algorithm
exhibits a good speed-up up to 64 cores. The speed-up is degraded for trees with
fewer nodes. However, even in such cases, our algorithm reaches good speed-up up
to 32 cores. Additionally, our algorithm outperforms the state-of-the-art
implementations using only $p=2$ threads. Considering all of this, the {\tt
  psta} algorithm is a good option to construction succinct trees in commodity
multicore architectures.

With respect to the working space, our {\tt psta} algorithm is competitive with
{\tt sdsl} and it does not use more than three times the memory used by {\tt
  libcds}, which is the slowest algorithm. Despite our algorithm using more
memory than {\tt sdsl} and {\tt libcds}, it is up to 20 times and 376 times
faster with 64 cores, respectively.

The scalability of the {\tt PFEA} algorithm up to 16 threads is good in practice
(nearly 50\% efficiency). However, the lack of workload of our algorithm
prevents it from obtaining a good practical scalability with more threads. In
this kind of 
algorithms, we cannot expect a better scalability adding more
threads. Nevertheless, this poses an interesting problem: For a given
algorithm, find the maximum number of threads that achieve at least a 50\%
efficiency. Once we find such number, the rest of the threads may be
used potentially in other procedures. The implementation of
multicore algorithms is non-trivial, since it needs to take care about the
communication costs, memory hierarchy, cache coherency, etc, which may affect
the performance. Therefore, we consider a parallel algorithm with a 50\%
efficiency a good parallel implementation.

It is important to emphasize that this work involves what we think
are important contributions to practical implementations of parallel
algorithms in commodity architectures. In a way, then, topology has
become important again, though thankfully not a show-stopper. For
example, in the Figures \ref{fig:speedup-seq}, \ref{fig:speedup-sdsl} and
\ref{fig:speedup-pfea}, we observed that algorithms had a slowdown
of the speedup at 16, 32, 48 and 64 threads. We hypothesize that the
factor that generated the slowdown of the speedups of the algorithms
had to do with the topology of the machine where we ran our
experiments. The four processors on our machine were connected in a
grid topology~\cite{Drepper2007}. This increases the cost of communication
between threads on these processors. Additionally, there exists other
factors of the architecture that can impact the performance of
multicore algorithms to construct succinct data structures, such as,
cache inclusion policy which may vary for each new architecture,
special wiring among cores and among caches, and cache coherency
protocol. The impact of all of these factors in the implementation of
multicore construction algorithms need to be studied in more detail.
