In this section, we describe our new parallel algorithm for constructing the
{\tt RMMT} of a given tree, called the \emph{Parallel Succinct Tree Algorithm}
({\tt PSTA}).  Its input is the balanced parenthesis sequence $P$ of an $n$-node
tree $T$.  This is a tree representation commonly used in practice, particularly
in secondary storage. For trees whose
folklore encoding is not directly available, in Section \ref{subsec:PFEA} we
describe a parallel algorithm that can compute such an encoding in $O(n/p + \lg
p)$ time.  Our algorithms assume that manipulating $w$ bits takes constant time.
Additionally, we assume the (time and space) overhead of scheduling threads on
cores is negligible. This is guaranteed by the results
of \cite{Blumofe:1999:SMC:324133.324234}, and the number of available processing
units in current systems is generally much smaller than the input size $n$, so
this cost is indeed negligible in practice.

\subsection{Parallel Folklore Encoding Algorithm}
\label{subsec:PFEA}

\begin{algorithm}[t]
  \small
  % keywords
  \SetKwInOut{Input}{Input}
  \SetKwInOut{Output}{Output}
  \SetKwFor{PFor}{parfor}{do}{end}
  \LinesNumbered
  \DontPrintSemicolon
  \SetVlineSkip{0.5ex}
  \SetCommentSty{textit}
  % I/o
  \Input{An adjacency list representation of $T$ consisting of arrays $V$ and $E$ and the number of threads, $\threads$.}
  \Output{The balanced parenthesis sequence $P$ of $T$.}
  \BlankLine
  % algorithm
  $\id{ET} \asgn {}$an array of length $2|E|$\;
  $\id{P} \asgn {}$an array of length $2|E|+2$\;
  $\id{chk} \asgn |E|/\threads$\;
  \PFor{$t \asgn 0$ \KwTo $\threads-1$}{
    \For{$i \asgn 0$ \KwTo $\chk-1$}{
      $j \asgn t*\chk + i$\;
      $\id{ET}[2*j].\id{value} \asgn 1$ \tcp*[h]{forward edge, opening parenthesis}\;
      $\id{ET}[2*j+1].\id{value} \asgn 0$ \tcp*[h]{backward edge, closing parenthesis}\;
      \eIf{$E[j].\chld$ is a leaf}{
        $\id{ET}[2*j].\id{succ} \asgn 2*j+1$\;
      }
      {
        $\id{ET}[2*j].\id{succ} \asgn 2*\id{next}(E[j].\chld)$\;
      }
      \eIf{$E[j]$ is the last edge in the adjacency list of $E[j].\parent$}{
        $\id{ET}[2*j+1].\id{succ} \asgn 2*\id{first}(E[j].\parent)+1$\;
      }
      {
        $\id{ET}[2*j+1].\id{succ} \asgn 2*\id{next}(E[j].\parent)$\;
      }
    }
  }
  $\id{parallel\_list\_ranking}(\id{ET})$\;
  \PFor{$t \asgn 0$ \KwTo $\threads - 1$}{
    \For{$i \asgn 0$ \KwTo $2*\chk-1$}{
      $P[\id{ET}[2*t*\chk+i+1].\id{rank}] \asgn \id{ET}[2*t*\chk+i+1].\id{value}$\;
    }
  }
  $P[0] \asgn 1$\;
  $P[2|E|+1] \asgn 0$\;
  \vspace{1ex}
  \caption{{\tt PFEA}}
  \label{algo:PFEA}
  \end{algorithm}

The {\tt PSTA} algorithm requires the balanced parentheses representation $P$ of
the input tree $T$, but in some applications $T$ may not be
given in this form. Here, we present a parallel algorithm that constructs the
balanced parenthesis sequence of $T$ from a representation of $T$ stored in
adjacency list representation. Since the balanced parenthesis sequence of $T$
is also known as its \emph{folklore encoding}, we call the algorithm the
\emph{Parallel Folklore Encoding Algorithm} ({\tt PFEA}).
The input tree is represented by an array of nodes, $V$, and an array of edges,
$E$. Each node $v$ in $V$ stores a pointer to an adjacency list with one entry
per edge incident to $v$, sorted counterclockwise around $v$, starting with
$v$'s parent edge. Each entry in this adjacency list points to $v$ and to the
edge in $E$ it represents. Each edge $e=(u,v)$ in $E$ points to its
corresponding entries in the adjacency lists of $u$ and $v$. Edges are assumed
to be directed from parents to children. Thus, for an edge $e = (u, v)$, we
refer to $u$ and $v$ as $e.\parent$ and $e.\chld$, respectively. For
$x \in \{u, v\}$, we use $\id{next}(e.x)$ and $\id{first}(e.x)$ to denote the
indices in $E$ of $e$'s successor and of the first element in $x$'s adjacency
list, respectively. Both are easily computed in constant time by following
pointers.

The idea behind the construction is the following: Given an Euler tour of $T$
that visits the children of each node in left-to-right order, the balanced
parenthesis representation of $T$ can be obtained by following the Euler tour,
writing down an opening parenthesis for every edge traversed from parent to child
and a closing parenthesis for every edge traversed from child to parent, and
finally enclosing the resulting sequence in a pair of parentheses representing
the root of $T$.

Algorithm~\ref{algo:PFEA} shows the pseudo-code of the construction. It creates
two arrays, one an auxiliary array $\id{ET}$ of length $2|E|$ to store the Euler
tour of $T$, the other an array $P$ of size $2|E|+2$ to store the balanced
parenthesis representation of $T$ (lines 1--2).  Each entry in $\id{ET}$
represents the traversal of an edge of $T$ and stores three values: $\id{value}$
is ``(`` or ``)'' depending on whether the edge is traversed from parent to
child or from child to parent, that is, it's the corresponding parenthesis to be
added to $P$; $\id{succ}$ is the index in $\id{ET}$ of the next edge in the
Euler tour; and $\id{rank}$ is the rank in the Euler tour. Lines~4--16 of the
algorithm populate $\id{ET}$ with entries representing the Euler tour but
leaving the $\id{rank}$ values uninitialized. Line~17 computes ranks using a
parallel list ranking algorithm~\cite{Helman2001265}. Given these ranks, the
balanced parenthesis representation can be obtained by writing
$\id{ET}[i].\id{value}$ into $P[\id{ET}[i].\id{rank}]$. Lines~18--22 do exactly
this.

\subsection{Parallel Succinct Tree Algorithm}
\label{subsec:PSTA}

\begin{figure}[t!]
  \begin{minipage}[t]{.50\textwidth}
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}
    \SetKwFor{PFor}{parfor}{do}{end}
    \LinesNumbered
    \DontPrintSemicolon
    \SetVlineSkip{0.5ex}
    \SetCommentSty{textit}
    \vspace{0pt}
    \begin{algorithm}[H]
      \small
      % I/o
      \Input{$P$, $s$, $\threads$}
      \Output{{\tt RMMT} represented as arrays $e', m', M', n'$ and universal
        lookup tables}
      \BlankLine
      % algorithm
      $o \asgn \lceil 2n/s \rceil-1$ \tcp*[h]{\# internal nodes}\;
      $e' \asgn {}$array of size $\lceil 2n/s \rceil$\;
      $m', M', n' \asgn {}$arrays of size $\lceil 2n/s \rceil + o$\;
      $\ct \asgn \lceil 2n/s \rceil/\threads$\;
      \PFor{$t \asgn 0$ \KwTo $\threads-1$}{
        $e'_t, m'_t, M'_t, n'_t \asgn 0$\;
        \For{$\chk \asgn 0$ \KwTo $\ct-1$}{
          $\low \asgn (t*\ct + \chk)*s$\;
          $\up \asgn \low+s$\;
          \For{$\pr \asgn \low$ \KwTo $\up-1$}{
            $e'_t \pasgn 2*P[\pr]-1$\;
            \uIf{$e'_t < m'_t$}{
              $m'_t \asgn e'_t$;
              $n'_t \asgn 1$
            }
            \uElseIf{$e'_t \eq m'_t$}{
              $n'_t \pasgn 1$\;
            }
            \ElseIf{$e'_t > M'_t$}{
              $M'_t \asgn e'_t$\;
            }
          }
          $e'[t*\ct+\chk] \asgn e'_t$\;
          $m'[t*\ct+\chk+o] \asgn m'_t$\;
          $M'[t*\ct+\chk+o] \asgn M'_t$\;
          $n'[t*\ct+\chk+o] \asgn n'_t$\;
        }
      }
      $\id{parallel\_prefix\_sum}(e', \ct)$\;
      \PFor{$t \asgn 1$ \KwTo $\threads-1$}{
        \For{$\chk \asgn 0$ \KwTo $\ct-1$}{
          \If{$\chk < \ct-1$}{
            $e'[t*\ct+\chk] \pasgn e'[t*\ct-1]$\;
          }
          $m'[t*\ct+\chk+o] \pasgn e'[t*\ct-1]$\;
          $M'[t*\ct+\chk+o] \pasgn e'[t*\ct-1]$\;
        }
      }
      \vspace{1ex}
      \caption{{\tt PSTA} (part I)}
      \label{algo:PSTA1}
    \end{algorithm}
  \end{minipage}
  \begin{minipage}[t]{.51\textwidth}
    \SetKwInOut{Input}{Input}
    \SetKwFor{PFor}{parfor}{do}{end}
    \SetKw{KwDownTo}{downto}
    \LinesNumbered
    \DontPrintSemicolon
    \SetVlineSkip{0.5ex}
    \vspace{0pt}
    \begin{algorithm}[H]
      \small
      $lvl \asgn \lceil \lg \threads \rceil$\;
      \PFor{$\st \asgn 0$ \KwTo $2^{\lvl}-1$}{
        \For{$l \asgn \lceil\lg (2n/s)\rceil-1$ \KwDownTo $\lvl$}{
          \For{$d \asgn 0$ \KwTo $2^{l-\lvl}-1$}{
            $i \asgn d + 2^{l} - 1 +st*2^{l-\lvl}$\;
            $\id{concat}(i,m',M',n')$\;	  	
          }
        }
      }
      \For{$l \asgn \lvl-1$ \KwTo $0$}{
        \PFor{$d \asgn 0$ \KwTo $2^{l}-1$}{
          $i \asgn d + 2^{l}-1$\;
          $\id{concat}(i,m',M',n')$\;	  	
        }
      }
      \vspace{1ex}
      \caption{{\tt PSTA} (part II)}
      \label{algo:PSTA2}
    \end{algorithm}
    \medskip
    \begin{algorithm}[H]
      \small
      \PFor{$x \asgn -w$ \KwTo $w-1$}{
        \PFor{$y \asgn 0$ \KwTo $\sqrt{2^{w}}-1$}{
          $i \asgn ((x+w) << w)$ OR $w$\;
          $\id{near\_fwd\_pos}[i] \asgn w$\;
          $p, \id{excess} \asgn 0$\;
          \Repeat{$p \geq w$}{
            $\id{excess} \pasgn 1-2*((y \mathop{\mathrm{AND}} (1 << p)) = 0)$\;
            \If{$\id{excess} \eq x$}{
              $\id{near\_fwd\_pos}[i] \asgn p$\;
              \KwSty{break}\;
            }
            $p \pasgn 1$\;
          }
        }
      }
      \vspace{1ex}
      \caption{{\tt PSTA} (part III)}
      \label{algo:PSTA3}
    \end{algorithm}
    \medskip
    \begin{function}[H]
      % I/o
      \Input{$i$, $m'$, $M'$, $n'$}
      \BlankLine
      $m'[i] \asgn \min(m'[2i+1], m'[2i+2])$\;
      $M'[i] \asgn \max(M'[2i+1], M'[2i+2])$\;
      \uIf{$m'[2i+1]<m'[2i+2]$}{
        $n'[i] \asgn n'[2i+1]$\;
      }\uElseIf{$m'[2i+1]>m'[2i+2]$}{
        $n'[i] \asgn n'[2i+2]$\;
      }\ElseIf{$m'[2i+1] \eq m'[2i+2]$}{
        $n'[i] \asgn n'[2i+1] + n'[2i+2]$\;
      }
      \vspace{1ex}
      \caption{concat()}
      \label{func:concat}
    \end{function}
  \end{minipage}
\end{figure}

Before describing the {\tt PSTA} algorithm, we observe
that the entries in $e'$ corresponding to internal nodes of the
{\tt RMMT} need not be stored explicitly.  This is because the entry
of $e'$ corresponding to an internal node is equal to the entry that
corresponds to the last leaf descendant of this node; since the {\tt
  RMMT} is complete, we can easily locate this leaf in constant
time.  Thus, our algorithm treats $e'$ as an
array of length $\lceil 2n / s\rceil$ with one entry per leaf.
Our algorithm consists of three phases. In the first
phase (Algorithm~\ref{algo:PSTA1}), it computes the leaves of the {\tt RMMT},
i.e., the array $e'$,
as well as the entries of $m'$, $M'$ and $n'$
that correspond to leaves.  In the second phase (Algorithm~\ref{algo:PSTA2}),
the algorithm computes the
entries of $m'$, $M'$ and $n'$ corresponding
to internal nodes of the {\tt RMMT}.
In the third phase (Algorithm~\ref{algo:PSTA3}), it computes the universal
lookup tables used to answer queries.
The input to our algorithm consists of the balanced parenthesis sequence,
$P$, the size of each chunk, $s$, and the number of available threads,
$\threads$.

To compute the entries of arrays $e'$, $m'$, $M'$, and $n'$ corresponding
to the leaves of the {\tt RMMT} (Algorithm~\ref{algo:PSTA1}), we first
assign the same number of consecutive chunks, $\ct$, to each thread (line~4).
We call such a concatenation of chunks assigned to a single thread
a \emph{superchunk}.
For simplicity, we assume that the total number of chunks,
$\lceil 2n / s\rceil$, is divisible by $\threads$.
Each thread then computes the \emph{local} excess value of the last
position in each of its assigned chunks, as well as the minimum and
maximum local excess in each chunk, and the number of times the minimum
local excess occurs in each chunk (lines 8--17).
These values are stored in the entries of $e'$, $m'$, $M'$, and
$n'$ corresponding to this chunk (lines 18--21).
The local excess value of a position $i$ in $P$ is defined to be $\sumop(P,\pi,j,i)$,
where $j$ is the index of the first position of the superchunk containing
position $i$.
Note that the locations with minimum local excess in each chunk are the same as
the positions with minimum global excess because the difference between local
and global excess is exactly $\sumop(P,\pi,0,j-1)$.
Thus, the entries in $n'$ corresponding to leaves store their final values at the
end of the loop in lines 5--21, while the corresponding entries of $e'$, $m'$,
and $M'$ store \emph{local} excess values.

To convert the entries in $e'$ into global excess values, observe that the
global excess at the end of each superchunk equals the sum of the local excess
values at the ends of all superchunks up to and including this superchunk.
Thus, we use a parallel prefix sum algorithm~\cite{Helman2001265} in line~22
to compute the global excess values at the ends of all superchunks and store
these values in the corresponding entries of $e'$.
The remaining local excess values in $e'$, $m'$, and $M'$ can now be converted
into global excess values by increasing each by the global excess at the end
of the preceding superchunk.
Lines 23--28 do exactly this.

The computation of entries of $m'$, $M'$, and $n'$ (Algorithm~\ref{algo:PSTA2})
first chooses the level closest to the root that contains at least $\threads$
nodes and creates one thread for each such node $v$.
The thread associated with node $v$ calculates the $m'$, $M'$, and $n'$ values
of all nodes in the subtree rooted at $v$, by applying the function
$\id{concat}$ to the nodes in the subtree bottom up (lines 2--6).
The invocation of this function for a node computes its $m'$, $M'$, and $n'$
values from the corresponding values of its children.
With a scheduler that balances the work, such as a
work-stealing scheduler, cores have a similar workload.
Lines 7--10 apply a similar bottom-up strategy for computing the $m'$, $M'$,
and $n'$ values of the nodes in the top $\lvl$ levels, but they do this by
processing these levels sequentially and, for each level, processing the
nodes on this level in parallel.

Algorithm~\ref{algo:PSTA3} illustrates the construction of universal lookup
tables using the construction of the table $\id{near\_fwd\_pos}$ as an example.
This table is used to support the {\fwdsearch} operation (see
Section~\ref{subsec:suctrees}).
Other lookup tables can be constructed analogously.
As each entry in such a universal table can be computed independently, we can
easily compute them in parallel.
